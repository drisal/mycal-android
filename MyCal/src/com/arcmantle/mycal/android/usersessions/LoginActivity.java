package com.arcmantle.mycal.android.usersessions;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.concurrent.atomic.AtomicInteger;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.arcmantle.mycal.android.MyCalMainActivity;
import com.arcmantle.mycal.android.R;
import com.arcmantle.mycal.android.gcm.GcmConfig;
import com.arcmantle.mycal.android.util.Constant;
import com.arcmantle.mycal.android.util.Field;
import com.arcmantle.mycal.android.util.Form;
import com.arcmantle.mycal.android.util.FormUtils;
import com.arcmantle.mycal.android.util.validations.HasMinimumLength;
import com.arcmantle.mycal.android.util.validations.IsEmail;
import com.arcmantle.mycal.android.util.validations.NotEmpty;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class LoginActivity extends Activity {

	Button btnLogin;
	Button btnBackToRegister;
	TextView loginErrorMsg;
	EditText txtUsername, txtPassword;
	// Form used for validation
	private Form mForm;
	
	private static String KEY_SUCCESS = "success";
	private static String KEY_FAILED = "failed";
	private static String KEY_ERROR = "error";
	private static String KEY_STATUS = "status";
	private static String AUTH_TOKEN = "auth_token";
	private static String KEY_ERROR_MSG = "error_message";
	/**
	 * Tag used on log messages.
	 */
	static final String TAG = "GCM REGISTRATION WITH LOGIN";

	String gcm_regid = "";
	GoogleCloudMessaging gcm;
	AtomicInteger msgId = new AtomicInteger();
	Context context;
	RequestQueue queue;
	// User Session Manager Class
	UserSessionManager session;
	JSONObject jObject;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		queue = Volley.newRequestQueue(this);
		// User Session Manager
		session = new UserSessionManager(getApplicationContext());
		// get Email, Password input texts

		Toast.makeText(getApplicationContext(),
				"User Login Status: " + session.isUserLoggedIn(),
				Toast.LENGTH_LONG).show();

		// GCM registration.
		if (GcmConfig.checkPlayServices(this, LoginActivity.this)) {
			gcm = GoogleCloudMessaging.getInstance(this);
			// gcm_regid = getRegistrationId(context);
		} else {
			Log.i(TAG, "No valid Google Play Services APK found.");
		}
		initFields();
		initValidationForm();
		initCallbacks();

	}

	private void initFields() {
		txtUsername = (EditText) findViewById(R.id.loginUsername);
		txtPassword = (EditText) findViewById(R.id.loginPassword);
		loginErrorMsg = (TextView) findViewById(R.id.login_error);
		btnBackToRegister = (Button) findViewById(R.id.btnLinkToRegisterScreen);
		// User Login button
		btnLogin = (Button) findViewById(R.id.btnLogin);
	}

	private void initValidationForm() {
		mForm = new Form(this);
		mForm.addField(Field.using(txtUsername).validate(NotEmpty.build(this)));
		mForm.addField(Field.using(txtPassword).validate(
				HasMinimumLength.build(this, 8)));
	}

	private boolean isEmail(String name) {
		return name.matches(IsEmail.EMAIL_PATTERN);
	}

	private void initCallbacks() {
		// Login button click event
		btnLogin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// Get username/email, password from EditText
				String username = txtUsername.getText().toString();
				String password = txtPassword.getText().toString();
				jObject = new JSONObject();
				try {
					if (isEmail(username)) {
						jObject.put("email", username);
					} else {
						jObject.put("username", username);
					}
					jObject.put("password", password);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				if (submit()) {
					// Validate if username, password is filled
					if (username.trim().length() > 0
							&& password.trim().length() > 0) {
						new LogIn()
								.execute(Constant.BASE_URL+"/api/v1/users/authenticate.json");
					} else {
						// user didn't entered username or password
						Toast.makeText(getApplicationContext(),
								"Please enter username and password",
								Toast.LENGTH_LONG).show();
					}
				}

			}
		});

		btnBackToRegister.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent i = new Intent(getApplicationContext(),
						RegisterActivity.class);
				startActivity(i);
				// Close Registration View
				finish();
			}
		});
	}

	private boolean submit() {
		FormUtils.hideKeyboard(LoginActivity.this, btnLogin);
		if (mForm.isValid()) {
			// Crouton.makeText(this,
			// getString(R.string.sample_activity_form_is_valid),
			// Style.CONFIRM).show();
			Toast.makeText(this, "Validation Pass", Toast.LENGTH_LONG).show();
			return true;
		}
		return false;
	}

	// Class with extends AsyncTask class

	public class LogIn extends AsyncTask<String, Void, Void> {

		// Required initialization
		// private final HttpClient Client = new DefaultHttpClient();
		private String Content;
		private String Error = null;
		private ProgressDialog Dialog = new ProgressDialog(LoginActivity.this);
		String data = "";
		int sizeData = 0;

		protected void onPreExecute() {
			// NOTE: You can call UI Element here.
			// Start Progress Dialog (Message)
			Dialog.setMessage("Please wait..");
			Dialog.show();
			try {
				// Set Request parameter
				data += "&" + URLEncoder.encode("user", "UTF-8") + "="
						+ jObject.toString();

			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {
			/************ Make Post Call To Web Server ***********/
			// Send POST data request
			Content = makeGetPostRequest(urls[0], data);

			/*****************************************************/
			return null;
		}

		protected void onPostExecute(Void unused) {
			// NOTE: You can call UI Element here.

			// Close progress dialog
			Dialog.dismiss();

			if (Error != null) {
				loginErrorMsg.setText("Output : " + Error);
			} else {
				/****************** Start Parse Response JSON Data *************/
				// String OutputData = "";
				// Check device for Play Services APK. If check succeeds,
				// proceed with
				JSONObject jsonResponse;
				// check for register response
				try {
					// try parse the string to a JSON object
					/******
					 * Creates a new JSONObject with name/value mappings from
					 * the JSON string.
					 ********/
					jsonResponse = new JSONObject(Content);
					if (jsonResponse.getString(KEY_STATUS).equalsIgnoreCase(
							KEY_SUCCESS)) {
						loginErrorMsg.setText("");
						// user successfully registred
						// Store user details in SQLite Database
						String auth_key = jsonResponse.getString(AUTH_TOKEN);
						UserSessionManager userSession = new UserSessionManager(
								getApplicationContext());
						// Clear all previous data
						if (userSession.isUserLoggedIn()) {
							userSession.clearData();
						}

						userSession.createUserLoginSession(auth_key);
						if (getRegistrationId(getApplicationContext()).isEmpty())
							registerInBackground(auth_key);
						// Starting MainActivity
						Intent i = new Intent(getApplicationContext(),
								MyCalMainActivity.class);
						i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						// Add new Flag to start new Activity
						i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(i);
						finish();
					} else {
						// Error in registration
						loginErrorMsg.setText(jsonResponse
								.getString(KEY_ERROR_MSG));
					}

				} catch (JSONException e) {
					Log.e("JSON Parser", "Error parsing data " + e.toString());
				}
			}
		}

	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and the app versionCode in the application's
	 * shared preferences.
	 */
	private void registerInBackground(final String auth_key) {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(context);
					}

					for (int i = 0; i < 5; i++) {
						if (gcm_regid.isEmpty())
							gcm_regid = gcm
									.register(GcmConfig.GOOGLE_SENDER_ID);
						else
							break;
					}

					msg = "Device registered, registration ID=" + gcm_regid;
					updateRegistrationIdToBackend(auth_key);
				} catch (Exception ex) {
					msg = "Error :" + ex.getMessage();
					registerInBackground(auth_key);
				}
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
				Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG)
						.show();
			}
		}.execute(null, null, null);
	}

	public void updateRegistrationIdToBackend(String auth_key) {
		String data = "";
		JSONObject jObj = new JSONObject();
		try {
			jObj.put("gcm_regid", gcm_regid);
			jObj.put("auth_token", auth_key);
			data += "&" + URLEncoder.encode("user", "UTF-8") + "="
					+ jObj.toString();			
			updateGCMRegId(data);
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

	}

	public String makeGetPostRequest(String urls, String data) {
		BufferedReader reader = null;
		StringBuilder sb = null;
		try {
			// Defined URL where to send data
			URL url = new URL(urls);
			// Send POST data request
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter wr = new OutputStreamWriter(
					conn.getOutputStream());
			wr.write(data);
			wr.flush();
			// Get the server response
			reader = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			sb = new StringBuilder();
			String line = null;
			// Read Server Response
			while ((line = reader.readLine()) != null) {
				// Append server response in string
				sb.append(line + "");
			}

		} catch (Exception ex) {
			ex.getMessage();
		} finally {
			try {
				reader.close();
			} catch (Exception ex) {
			}
		}
		return sb.toString();

	}

	/**
	 * Gets the current registration ID for application on GCM service, if there
	 * is one.
	 * <p>
	 * If result is empty, the app needs to register.
	 * 
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	private String getRegistrationId(Context context) {
		final SharedPreferences prefs = getGcmPreferences(context);
		String registrationId = prefs.getString(GcmConfig.PROPERTY_REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i(TAG, "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(GcmConfig.PROPERTY_APP_VERSION,
				Integer.MIN_VALUE);
		int currentVersion = GcmConfig.getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(TAG, "App version changed.");
			return "";
		}
		return registrationId;
	}

	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private SharedPreferences getGcmPreferences(Context context) {
		// This sample app persists the registration ID in shared preferences,
		// but
		// how you store the regID in your app is up to you.
		return getSharedPreferences(GcmConfig.PREFER_NAME, Context.MODE_PRIVATE);
	}
	
	
	public void updateGCMRegId(String data){
		final ProgressDialog pDialog = new ProgressDialog(this);
		pDialog.setMessage("Updating Gcm RegId...");
		pDialog.show();     
		         
		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.PUT,
				GcmConfig.USER_INFO_BY_ID_URL, data, new Response.Listener<JSONObject>() {		 
		                    @Override
		                    public void onResponse(JSONObject response) {
		                        Log.d(TAG, response.toString());
		                        pDialog.hide();
		                    }
		                }, new Response.ErrorListener() {		 
		                    @Override
		                    public void onErrorResponse(VolleyError error) {
		                        VolleyLog.d(TAG, "Error: " + error.getMessage());
		                        pDialog.hide();
		                    }							
		                });
		 
		// Adding request to request queue
		queue.add(jsonObjReq);

	}

}