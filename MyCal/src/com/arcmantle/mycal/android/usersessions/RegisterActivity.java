package com.arcmantle.mycal.android.usersessions;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.arcmantle.mycal.android.MyCalMainActivity;
import com.arcmantle.mycal.android.R;
import com.arcmantle.mycal.android.gcm.GcmConfig;
import com.arcmantle.mycal.android.util.Constant;
import com.arcmantle.mycal.android.util.Field;
import com.arcmantle.mycal.android.util.Form;
import com.arcmantle.mycal.android.util.FormUtils;
import com.arcmantle.mycal.android.util.validations.HasMinimumLength;
import com.arcmantle.mycal.android.util.validations.IsEmail;
import com.arcmantle.mycal.android.util.validations.IsPositiveInteger;
import com.arcmantle.mycal.android.util.validations.NotEmpty;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class RegisterActivity extends Activity {
	Button btnRegister;
	Button btnLinkToLogin;
	EditText inputUsername;
	EditText inputFullName;
	EditText contactNumb;
	EditText inputEmail;
	EditText inputPassword;
	EditText confirmPassword;
	TextView registerErrorMsg;
	// Form used for validation
	private Form mForm;
	JSONObject jObject;
	// JSON Response node names

	// Controller aController;
	String gcm_regid = "";
	private static String KEY_SUCCESS = "success";
	private static String KEY_FAILED = "failed";
	private static String KEY_ERROR = "error";
	private static String KEY_ERROR_MSG = "error_message";
	private static String AUTH_TOKEN = "auth_token";
	private static String KEY_STATUS = "status";
	

	public static final String EXTRA_MESSAGE = "message";	
	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	/**
	 * Tag used on log messages.
	 */
	static final String TAG = "GCM REGISTRATION WITH REGISTRATION";

	GoogleCloudMessaging gcm;
	AtomicInteger msgId = new AtomicInteger();
	Context context;

	String regid;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);
		context = getApplicationContext();

		// Check device for Play Services APK. If check succeeds, proceed with
		// GCM registration.
		if (GcmConfig.checkPlayServices(this,RegisterActivity.this)) {
			gcm = GoogleCloudMessaging.getInstance(this);			
				
		} else {
			Log.i(TAG, "No valid Google Play Services APK found.");
		}

		initFields();
		initValidationForm();
		initCallbacks();
	}

	private void initFields() {
		// Importing all assets like buttons, text fields
		inputUsername = (EditText) findViewById(R.id.username);
		inputFullName = (EditText) findViewById(R.id.fullName);
		inputEmail = (EditText) findViewById(R.id.registerEmail);
		contactNumb = (EditText) findViewById(R.id.contactnumber);
		inputPassword = (EditText) findViewById(R.id.registerPassword);
		confirmPassword = (EditText) findViewById(R.id.confirmPassword);
		btnRegister = (Button) findViewById(R.id.btnRegister);
		btnLinkToLogin = (Button) findViewById(R.id.btnLinkToLoginScreen);
		registerErrorMsg = (TextView) findViewById(R.id.register_error);
	}

	private void initValidationForm() {
		mForm = new Form(this);
		mForm.addField(Field.using(inputUsername)
				.validate(NotEmpty.build(this)));
		mForm.addField(Field.using(inputFullName)
				.validate(NotEmpty.build(this)));
		mForm.addField(Field.using(inputEmail).validate(NotEmpty.build(this))
				.validate(IsEmail.build(this)));
		mForm.addField(Field.using(contactNumb).validate(NotEmpty.build(this))
				.validate(IsPositiveInteger.build(this)));
		mForm.addField(Field.using(inputPassword).validate(
				HasMinimumLength.build(this, 8)));
		mForm.addField(Field.using(confirmPassword).validate(
				HasMinimumLength.build(this, 8)));
	}

	private void initCallbacks() {
		// Register Button Click event
		btnRegister.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {				
				registrationButtonPressed();
			}
		});

		// Link to Login Screen
		btnLinkToLogin.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent i = new Intent(getApplicationContext(),
						LoginActivity.class);
				startActivity(i);
				// Close Registration View
				finish();
			}
		});
	}

	private void submit() {
		FormUtils.hideKeyboard(RegisterActivity.this, btnRegister);
		if (mForm.isValid()) {
			// Crouton.makeText(this,
			// getString(R.string.sample_activity_form_is_valid),
			// Style.CONFIRM).show();
			Toast.makeText(this, "Validation Pass", Toast.LENGTH_LONG).show();			
			new Registration()
					.execute(Constant.BASE_URL+"/api/v1/users.json");
		}
	}

	// Class with extends AsyncTask class

	public class Registration extends AsyncTask<String, Void, Void> {

		// Required initialization
		private final HttpClient Client = new DefaultHttpClient();
		private String Content;
		private String Error = null;
		private ProgressDialog Dialog = new ProgressDialog(
				RegisterActivity.this);
		String data = "";
		int sizeData = 0;

		protected void onPreExecute() {
			// NOTE: You can call UI Element here.
			// Start Progress Dialog (Message)
			Dialog.setMessage("Please wait..");
			Dialog.show();
			try {
				// Set Request parameter
				data += "&" + URLEncoder.encode("user", "UTF-8") + "="
						+ jObject.toString();

			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {
			/************ Make Post Call To Web Server ***********/			
				// Send POST data request
				Content = makeGetPostRequest(urls[0], data);		
			/*****************************************************/
			return null;
		}

		protected void onPostExecute(Void unused) {
			// NOTE: You can call UI Element here.
			// Close progress dialog
			Dialog.dismiss();

			if (Error != null) {
				registerErrorMsg.setText("Output : " + Error);
			} else {
				/****************** Start Parse Response JSON Data *************/
				String OutputData = "";
				JSONObject jsonResponse;
				// check for register response
				try {
					// try parse the string to a JSON object
					/******
					 * Creates a new JSONObject with name/value mappings from
					 * the JSON string.
					 ********/
					jsonResponse = new JSONObject(Content);
					if (jsonResponse.getString(KEY_STATUS).equalsIgnoreCase(
							KEY_SUCCESS)) {
						registerErrorMsg.setText("");
						// user successfully registred
						// Store user details in SQLite Database
						String auth_key = jsonResponse.getString(AUTH_TOKEN);
						UserSessionManager userSession = new UserSessionManager(
								getApplicationContext());
						// Clear all previous data
						if (userSession.isUserLoggedIn()) {
							userSession.clearData();
						}

						userSession.createUserLoginSession(auth_key);
						// Starting MainActivity
						Intent i = new Intent(getApplicationContext(),
								MyCalMainActivity.class);
						i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						// Add new Flag to start new Activity
						i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(i);
						finish();
					} else {
						// Error in registration
						registerErrorMsg.setText(jsonResponse
								.getString(KEY_ERROR_MSG));
					}

				} catch (JSONException e) {
					Log.e("JSON Parser", "Error parsing data " + e.toString());
				}
			}
		}

	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If it
	 * doesn't, display a dialog that allows users to download the APK from the
	 * Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i(TAG, "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}

	/**
	 * Stores the registration ID and the app versionCode in the application's
	 * {@code SharedPreferences}.
	 * 
	 * @param context
	 *            application's context.
	 * @param regId
	 *            registration ID
	 */
	private void storeRegistrationId(Context context, String regId) {
		final SharedPreferences prefs = getGcmPreferences(context);
		int appVersion = GcmConfig.getAppVersion(context);
		Log.i(TAG, "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(GcmConfig.PROPERTY_REG_ID, regId);
		editor.putInt(GcmConfig.PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}

	

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and the app versionCode in the application's
	 * shared preferences.
	 */
	private void registerInBackground() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(context);
					}
					for (int i = 0; i < 5; i++) {
						if (gcm_regid.isEmpty())
							gcm_regid = gcm.register(GcmConfig.GOOGLE_SENDER_ID);
						else
							break;
					}

					msg = "Device registered, registration ID=" + gcm_regid;

					storeRegistrationId(context, gcm_regid);
					// catch IOException
				} catch (Exception ex) {
					msg = "Error :" + ex.getMessage();
					// If there is an error, don't just keep trying to register.
					// Require the user to click a button again, or perform
					// exponential back-off.
				}
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
				Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG)
						.show();
			}
		}.execute(null, null, null);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	

	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private SharedPreferences getGcmPreferences(Context context) {
		// This sample app persists the registration ID in shared preferences,
		// but
		// how you store the regID in your app is up to you.
		return getSharedPreferences(GcmConfig.PREFER_NAME, Context.MODE_PRIVATE);
	}	
	
	public String makeGetPostRequest(String urls, String data) {
		BufferedReader reader = null;
		StringBuilder sb=null;
		try {
			// Defined URL where to send data
			URL url = new URL(urls);
			// Send POST data request
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter wr = new OutputStreamWriter(
					conn.getOutputStream());
			wr.write(data);
			wr.flush();
			// Get the server response
			reader = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			sb = new StringBuilder();
			String line = null;
			// Read Server Response
			while ((line = reader.readLine()) != null) {
				// Append server response in string
				sb.append(line + "");
			}
			
		} catch (Exception ex) {
			ex.getMessage();
		} finally {
			try {
				reader.close();				
			} catch (Exception ex) {
			}
		}
		return sb.toString();
	}
	
	public void registrationButtonPressed(){
		registerInBackground();
		String username = inputUsername.getText().toString();
		String fullname = inputFullName.getText().toString();
		String email = inputEmail.getText().toString();
		String contactnumb = contactNumb.getText().toString();
		String password = inputPassword.getText().toString();
		String confirmpassword = confirmPassword.getText().toString();

		jObject = new JSONObject();
		try {
			jObject.put("username", username);
			jObject.put("full_name", fullname);
			jObject.put("email", email);
			jObject.put("contact_number", contactnumb);
			jObject.put("password", password);
			jObject.put("password_confirmation", confirmpassword);
			jObject.put("gcm_regid", gcm_regid);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		submit();
	}
	public void gcmRegFailed(){
		new AlertDialog.Builder(RegisterActivity.this)
		.setIcon(android.R.drawable.ic_dialog_alert)
		.setTitle("GCM Registration Failed")
		.setMessage("Please Click Again for GCM Registartion!!!")
		.setPositiveButton(R.string.event_view_deletion_dialog_yes,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog,
							int which) {
						registrationButtonPressed();
					}

				})
		.setNegativeButton(R.string.event_view_deletion_dialog_no, null)
		.show();
	}
}