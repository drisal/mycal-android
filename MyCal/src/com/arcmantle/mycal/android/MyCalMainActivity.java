package com.arcmantle.mycal.android;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.Toast;

import com.arcmantle.mycal.android.event.EventListActivity;
import com.arcmantle.mycal.android.gcm.GcmConfig;
import com.arcmantle.mycal.android.usersessions.LoginActivity;
import com.arcmantle.mycal.android.usersessions.UserSessionManager;
import com.arcmantle.mycal.android.util.ConnectionDetector;
import com.arcmantle.mycal.android.util.JSONParser;

@SuppressWarnings("deprecation")
public class MyCalMainActivity extends TabActivity {
	UserSessionManager userSessions;
	// Refresh menu item
	private MenuItem refreshMenuItem;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Check login status in database
		DetectConnection();

		userSessions = new UserSessionManager(getApplicationContext());
		if (userSessions.isUserLoggedIn()) {
			setContentView(R.layout.activity_main);
			Resources ressources = getResources();
			TabHost tabHost = getTabHost();
			// Month tab
			Intent intentMonth = new Intent(this, MonthActivity.class);
			TabSpec tabSpecMonth = tabHost.newTabSpec("Month")
					.setIndicator("Month").setContent(intentMonth);
			// Week tab
			Intent intentWeek = new Intent(this, WeekActivity.class);
			TabSpec tabSpecWeek = tabHost.newTabSpec("Week")
					.setIndicator("Week").setContent(intentWeek);
			// Day tab
			Intent intentDay = new Intent(this, DayActivity.class);
			TabSpec tabSpecDay = tabHost.newTabSpec("Day").setIndicator("Day")
					.setContent(intentDay);
			// Event List tab
			Intent intentShowEvent = new Intent(this, EventListActivity.class);
			TabSpec tabSpecShowEvent = tabHost.newTabSpec("Event")
					.setIndicator("Event").setContent(intentShowEvent);

			// add all tabs
			tabHost.addTab(tabSpecMonth);
			tabHost.addTab(tabSpecWeek);
			tabHost.addTab(tabSpecDay);
			tabHost.addTab(tabSpecShowEvent);

			// set Windows tab as default (zero based)
			tabHost.setCurrentTab(0);
		} else {
			// user is not logged in redirect him to Login Activity
			Intent i = new Intent(this, LoginActivity.class);
			// Closing all the Activities from stack
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			// Add new Flag to start new Activity
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			// Staring Login Activity
			this.startActivity(i);
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.my_cal_main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		if (itemId == R.id.action_settings) {
			// search action
			return true;
		} else if (itemId == R.id.logout_action) {
			// search action
			logout();
			return true;
		} else if (itemId == R.id.action_connection_detect) {
			// location found
			DetectConnection();
			return true;
		} else if (itemId == R.id.event_dataRefresh) {
			// refresh
			refreshMenuItem = item;
			// WebServer Request URL
			String[] userURL = { GcmConfig.GET_USER_INFO_URL };
			String[] eventURL = { GcmConfig.GET_APPOINTENET_URL };
			// load the data from server
			new SyncUserData().execute(userURL);
			new SyncEventData().execute(eventURL);
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	public void DetectConnection() {
		// flag for Internet connection status
		Boolean isInternetPresent = false;

		// creating connection detector class instance
		ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
		// get Internet status
		isInternetPresent = cd.isConnectingToInternet();

		// check for Internet status
		if (isInternetPresent) {
			// Internet Connection is Present
			// make HTTP requests
			// showAlertDialog(MyCalMainActivity.this,
			// "Internet Connection","You have internet connection", true);
			Toast.makeText(getApplicationContext(),
					"Internet Connection :" + "You have internet connection",
					Toast.LENGTH_LONG).show();
		} else {
			// Internet connection is not present
			// Ask user to connect to Internet
			showAlertDialog(MyCalMainActivity.this, "No Internet Connection",
					"You don't have internet connection.", false);
			/*
			 * Toast.makeText( getApplicationContext(), "Internet Connection :"
			 * + "You have no internet connection", Toast.LENGTH_LONG).show();
			 */
		}

	}

	/**
	 * Function to display simple Alert Dialog
	 * 
	 * @param context
	 *            - application context
	 * @param title
	 *            - alert dialog title
	 * @param message
	 *            - alert message
	 * @param status
	 *            - success/failure (used to set icon)
	 * */
	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

		// Setting Dialog Title
		alertDialog.setTitle(title);

		// Setting Dialog Message
		alertDialog.setMessage(message);

		// Setting alert dialog icon
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

		// Setting OK Button
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		});

		// Showing Alert Message
		alertDialog.show();
	}

	/**
	 * Async task to load the data from server
	 * **/
	private class SyncUserData extends AsyncTask<String, Void, String> {
		private String Error = null;
		String dataReqForUser = "";
		private JSONArray jsonArray = null;
		JSONParser jsonParser = new JSONParser();

		@Override
		protected void onPreExecute() {
			// set the progress bar view
			refreshMenuItem.setActionView(R.layout.action_progressbar);
			refreshMenuItem.expandActionView();
			// Set Request parameter // data +="&" +
			try {
				dataReqForUser += "?"
						+ "page=0&per_page=0&auth_token="
						+ URLEncoder.encode(userSessions.getAuthToken(),
								"UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... urls) {
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse responseForUser;
			String responseString = null;
			/************ Make Post Call To Web Server ***********/
			// BufferedReader reader = null;
			try {
				urls[0] += dataReqForUser;
				responseForUser = httpclient.execute(new HttpGet(urls[0]));
				StatusLine statusLine = responseForUser.getStatusLine();
				if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					responseForUser.getEntity().writeTo(out);
					out.close();
					responseString = out.toString();
				} else {
					// Closes the connection.
					responseForUser.getEntity().getContent().close();
					throw new IOException(statusLine.getReasonPhrase());
				}
			} catch (ClientProtocolException e) {
				// TODO Handle problems..
			} catch (IOException e) {
				// TODO Handle problems..
			}
			return responseString;
		}

		protected void onPostExecute(String result) {
			if (Error != null) {
				// uiUpdate.setText("Output : " + Error);
			} else {
				// try parse the string to a JSON object
				try {
					jsonArray = new JSONArray(result);
					jsonParser.SyncJsonArrayToUserList(jsonArray,
							getApplicationContext());
				} catch (JSONException e) {
					Log.e("JSON Parser", "Error parsing data " + e.toString());
				}
			}
			refreshMenuItem.collapseActionView();
			// remove the progress bar view
			refreshMenuItem.setActionView(null);
		}
	}

	/**
	 * Async task to load the data from server
	 * **/
	private class SyncEventData extends AsyncTask<String, Void, String> {
		private String Error = null;
		String dataReqForEvent = "";
		private JSONArray jsonArray = null;
		JSONParser jsonParser = new JSONParser();

		// int sizeData = 0;

		@Override
		protected void onPreExecute() {
			// set the progress bar view
			refreshMenuItem.setActionView(R.layout.action_progressbar);
			refreshMenuItem.expandActionView();
			// Set Request parameter // data +="&" +
			try {
				dataReqForEvent += "?"
						+ "page=0&per_page=0&auth_token="
						+ URLEncoder.encode(userSessions.getAuthToken(),
								"UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		@Override
		protected String doInBackground(String... urls) {
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse responseForEvent;
			String responseString = null;
			/************ Make Post Call To Web Server ***********/
			// BufferedReader reader = null;
			try {
				urls[0] += dataReqForEvent;
				responseForEvent = httpclient.execute(new HttpGet(urls[0]));
				StatusLine statusLine = responseForEvent.getStatusLine();
				if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					responseForEvent.getEntity().writeTo(out);
					out.close();
					responseString = out.toString();
				} else {
					// Closes the connection.
					responseForEvent.getEntity().getContent().close();
					throw new IOException(statusLine.getReasonPhrase());
				}
			} catch (ClientProtocolException e) {
				// TODO Handle problems..
			} catch (IOException e) {
				// TODO Handle problems..
			}
			return responseString;
		}

		protected void onPostExecute(String result) {
			if (Error != null) {
				// uiUpdate.setText("Output : " + Error);
			} else {
				// try parse the string to a JSON object
				try {
					jsonArray = new JSONArray(result);
					if (jsonArray != null)
						jsonParser.SyncJsonArrayToEventList(jsonArray,
								getApplicationContext());
				} catch (JSONException e) {
					Log.e("JSON Parser", "Error parsing data " + e.toString());
				}
			}
			refreshMenuItem.collapseActionView();
			// remove the progress bar view
			refreshMenuItem.setActionView(null);
		}
	}

	public void logout() {
		userSessions.logoutUser();
		// Closing dashboard screen
		finish();
	}

}