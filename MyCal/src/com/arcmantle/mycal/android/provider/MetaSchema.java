package com.arcmantle.mycal.android.provider;

import android.content.ContentValues;
import android.content.UriMatcher;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * 
 * <p>
 * provides a very logical organization of the meta-data of the Database and
 * Content Provider
 * 
 * @author Deep Risal
 */

public class MetaSchema {

	/**
	 * Project Related Constants
	 */

	public static final String ORGANIZATIONAL_NAME = "com.arcmantle";
	public static final String PROJECT_NAME = "mycal.android";
	public static final String DATABASE_NAME = "mycal_db";
	public static final int DATABASE_VERSION = 1;

	/**
	 * ConentProvider Related Constants
	 */
	public static final String AUTHORITY = ORGANIZATIONAL_NAME + "."
			+ PROJECT_NAME + ".provider";
	private static final Uri BASE_URI = Uri.parse("content://" + AUTHORITY);
	public static final UriMatcher URI_MATCHER = buildUriMatcher();

	// register identifying URIs for Restaurant entity
	// the TOKEN value is associated with each URI registered
	private static UriMatcher buildUriMatcher() {

		// add default 'no match' result to matcher
		final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
		// ST:addMatcherURIs:inline
		// Event URIs
		matcher.addURI(AUTHORITY, Event.PATH, Event.PATH_TOKEN);
		matcher.addURI(AUTHORITY, Event.PATH_FOR_ID, Event.PATH_FOR_ID_TOKEN);
		matcher.addURI(AUTHORITY, Event.PATH_FOR_LIMIT_DATA,
				Event.PATH_FOR_LIMIT_DATA_TOKEN);

		// Users URIs
		matcher.addURI(AUTHORITY, Users.PATH, Users.PATH_TOKEN);
		matcher.addURI(AUTHORITY, Users.PATH_FOR_ID,
				Users.PATH_FOR_ID_TOKEN);
		matcher.addURI(AUTHORITY, Users.PATH_FOR_LIMIT_DATA,
				Users.PATH_FOR_LIMIT_DATA_TOKEN);
		// ST:addMatcherURIs:complete
		return matcher;

	}

	// Define a static class that represents description of stored content
	// entity.
	public static class Event {
		public static final String TABLE_NAME = "event_table";
		// define a URI paths to access entity
		// BASE_URI/event - for list of event(s)
		// BASE_URI/event/* - retrieve specific event by id
		public static final String PATH = TABLE_NAME;
		public static final int PATH_TOKEN = 110;

		public static final String PATH_FOR_ID = TABLE_NAME + "/*";
		public static final int PATH_FOR_ID_TOKEN = 120;

		public static final String PATH_FOR_LIMIT_DATA = TABLE_NAME + "/*/*";
		public static final int PATH_FOR_LIMIT_DATA_TOKEN = 130;

		// URI for all content stored as event entity
		public static final Uri CONTENT_URI = BASE_URI.buildUpon()
				.appendPath(PATH).build();

		private final static String MIME_TYPE_END = "event";

		// define the MIME type of data in the content provider
		public static final String CONTENT_TYPE_DIR = ORGANIZATIONAL_NAME
				+ ".cursor.dir/" + ORGANIZATIONAL_NAME + "." + MIME_TYPE_END;
		public static final String CONTENT_ITEM_TYPE = ORGANIZATIONAL_NAME
				+ ".cursor.item/" + ORGANIZATIONAL_NAME + "." + MIME_TYPE_END;

		// the names and order of ALL columns, including internal use ones
		public static final String[] ALL_COLUMN_NAMES = { Cols.ID, Cols.TITLE,
				Cols.DESCRIPTION, Cols.START_TIME, Cols.END_TIME,
				Cols.CREATION_TIME,Cols.UPDATE_TIME, Cols.LATITUDE, Cols.LONGITUDE,
				Cols.LOCATION, Cols.COLOR,Cols.CREATED_BY,Cols.OFF_ACTION};

		public static ContentValues initializeWithDefault(
				final ContentValues assignedValues) {
			// final Long now = Long.valueOf(System.currentTimeMillis());
			final ContentValues setValues = (assignedValues == null) ? new ContentValues()
					: assignedValues;
			if (!setValues.containsKey(Cols.TITLE)) {
				setValues.put(Cols.TITLE, "");
			}
			if (!setValues.containsKey(Cols.DESCRIPTION)) {
				setValues.put(Cols.DESCRIPTION, "");
			}
			if (!setValues.containsKey(Cols.START_TIME)) {
				setValues.put(Cols.START_TIME, "");
			}
			if (!setValues.containsKey(Cols.END_TIME)) {
				setValues.put(Cols.END_TIME, "");
			}
			if (!setValues.containsKey(Cols.CREATION_TIME)) {
				setValues.put(Cols.CREATION_TIME, "");
			}
			if (!setValues.containsKey(Cols.UPDATE_TIME)) {
				setValues.put(Cols.UPDATE_TIME, "");
			}
			if (!setValues.containsKey(Cols.LATITUDE)) {
				setValues.put(Cols.LATITUDE, 0);
			}
			if (!setValues.containsKey(Cols.LONGITUDE)) {
				setValues.put(Cols.LONGITUDE, 0);
			}
			if (!setValues.containsKey(Cols.LOCATION)) {
				setValues.put(Cols.LOCATION, "");
			}
			if (!setValues.containsKey(Cols.COLOR)) {
				setValues.put(Cols.COLOR, 0);
			}
			if (!setValues.containsKey(Cols.CREATED_BY)) {
				setValues.put(Cols.CREATED_BY, 0);
			}
			if (!setValues.containsKey(Cols.OFF_ACTION)) {
				setValues.put(Cols.OFF_ACTION, "N");
			}
			return setValues;
		}

		// a static class to store columns in entity
		public static class Cols {
			public static final String ID = BaseColumns._ID; // convention
			// The name and column index of each column in your database
			public static final String TITLE = "TITLE";
			public static final String DESCRIPTION = "DESCRIPTION";
			public static final String START_TIME = "START_TIME";
			public static final String END_TIME = "END_TIME";
			public static final String CREATION_TIME = "CREATION_TIME";
			public static final String UPDATE_TIME = "UPDATE_TIME";
			public static final String LATITUDE = "LATITUDE";
			public static final String LONGITUDE = "LONGITUDE";
			public static final String LOCATION = "LOCATION";
			public static final String COLOR = "COLOR_ID";
			public static final String CREATED_BY = "CREATED_BY";
			public static final String OFF_ACTION = "OFF_ACTION";
		}
	}

	// Define a static class that represents description of stored content
	// entity.
	public static class Users {
		public static final String TABLE_NAME = "invitee_table";

		// define a URI paths to access entity
		// BASE_URI/invitee - for list of invitee(s)
		// BASE_URI/invitee/* - retrieve specific invitee by id
		public static final String PATH = TABLE_NAME;
		public static final int PATH_TOKEN = 210;

		public static final String PATH_FOR_ID = TABLE_NAME + "/*";
		public static final int PATH_FOR_ID_TOKEN = 220;
		public static final String PATH_FOR_LIMIT_DATA = TABLE_NAME + "/*/*";
		public static final int PATH_FOR_LIMIT_DATA_TOKEN = 230;

		// URI for all content stored as Restaurant entity
		public static final Uri CONTENT_URI = BASE_URI.buildUpon()
				.appendPath(PATH).build();

		private final static String MIME_TYPE_END = "invities";

		// define the MIME type of data in the content provider
		public static final String CONTENT_TYPE_DIR = ORGANIZATIONAL_NAME
				+ ".cursor.dir/" + ORGANIZATIONAL_NAME + "." + MIME_TYPE_END;
		public static final String CONTENT_ITEM_TYPE = ORGANIZATIONAL_NAME
				+ ".cursor.item/" + ORGANIZATIONAL_NAME + "." + MIME_TYPE_END;

		// the names and order of ALL columns, including internal use ones
		public static final String[] ALL_COLUMN_NAMES = { Cols.ID, Cols.UID,
				Cols.EMAIL_ID, Cols.PHONE_NO, Cols.FIRST_NAME, Cols.LAST_NAME,
				Cols.CREATED_AT,Cols.AUTH_KEY };

		public static ContentValues initializeWithDefault(
				final ContentValues assignedValues) {
			// final Long now = Long.valueOf(System.currentTimeMillis());
			final ContentValues setValues = (assignedValues == null) ? new ContentValues()
					: assignedValues;
			if (!setValues.containsKey(Cols.UID)) {
				setValues.put(Cols.UID, "");
			}
			if (!setValues.containsKey(Cols.EMAIL_ID)) {
				setValues.put(Cols.EMAIL_ID, "");
			}
			if (!setValues.containsKey(Cols.PHONE_NO)) {
				setValues.put(Cols.PHONE_NO, "");
			}
			if (!setValues.containsKey(Cols.FIRST_NAME)) {
				setValues.put(Cols.FIRST_NAME, "");
			}
			if (!setValues.containsKey(Cols.LAST_NAME)) {
				setValues.put(Cols.LAST_NAME, "");
			}
			if (!setValues.containsKey(Cols.CREATED_AT)) {
				setValues.put(Cols.CREATED_AT, "");
			}
			if (!setValues.containsKey(Cols.IS_INVITEE)) {
				setValues.put(Cols.IS_INVITEE, false);
			}
			if (!setValues.containsKey(Cols.AUTH_KEY)) {
				setValues.put(Cols.AUTH_KEY, "");
			}
			return setValues;
		}

		// a static class to store columns in entity
		public static class Cols {
			public static final String ID = BaseColumns._ID; // convention
			// The name and column index of each column in your database
			public static final String UID = "UID";
			public static final String EMAIL_ID = "EMAIL_ID";
			public static final String PHONE_NO = "PHONE_NO";
			public static final String FIRST_NAME = "FIRST_NAME";
			public static final String LAST_NAME = "LAST_NAME";
			public static final String CREATED_AT = "CREATED_AT";
			public static final String IS_INVITEE = "IS_INVITEE";
			public static final String AUTH_KEY = "AUTH_KEY";
		}
	}

	// Define a static class that represents description of stored content
	// entity.
	public static class Event_Users {
		public static final String TABLE_NAME = "event_user_table";

		// define a URI paths to access entity
		// BASE_URI/event_invitee - for list of event_invitee(s)
		// BASE_URI/event_invitee/* - retrieve specific event_invitee by id
		public static final String PATH = "event_users";
		public static final int PATH_TOKEN = 310;

		public static final String PATH_FOR_ID = "event_users/*";
		public static final int PATH_FOR_ID_TOKEN = 320;

		// URI for all content stored as Restaurant entity
		public static final Uri CONTENT_URI = BASE_URI.buildUpon()
				.appendPath(PATH).build();

		private final static String MIME_TYPE_END = "event_users";

		// define the MIME type of data in the content provider
		public static final String CONTENT_TYPE_DIR = ORGANIZATIONAL_NAME
				+ ".cursor.dir/" + ORGANIZATIONAL_NAME + "." + MIME_TYPE_END;
		public static final String CONTENT_ITEM_TYPE = ORGANIZATIONAL_NAME
				+ ".cursor.item/" + ORGANIZATIONAL_NAME + "." + MIME_TYPE_END;

		// the names and order of ALL columns, including internal use ones
		public static final String[] ALL_COLUMN_NAMES = { Cols.ID,
				Cols.EVENT_ID, Cols.USER_ID };

		public static ContentValues initializeWithDefault(
				final ContentValues assignedValues) {
			// final Long now = Long.valueOf(System.currentTimeMillis());
			final ContentValues setValues = (assignedValues == null) ? new ContentValues()
					: assignedValues;
			if (!setValues.containsKey(Cols.EVENT_ID)) {
				setValues.put(Cols.EVENT_ID, 0);
			}
			if (!setValues.containsKey(Cols.USER_ID)) {
				setValues.put(Cols.USER_ID, 0);
			}
			return setValues;
		}

		// a static class to store columns in entity
		public static class Cols {
			public static final String ID = BaseColumns._ID; // convention
			// The name and column index of each column in your database
			public static final String EVENT_ID = "EVENT_ID";
			public static final String USER_ID = "USER_ID";
		}
	}

}
