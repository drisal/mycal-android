package com.arcmantle.mycal.android.provider;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import com.arcmantle.mycal.android.orm.Event;
import com.arcmantle.mycal.android.orm.EventCreator;
import com.arcmantle.mycal.android.orm.User;
import com.arcmantle.mycal.android.orm.UserCreator;

/**
 * This is the class that actually interacts with the SQLite3 database and does
 * the operations to manipulate the data within the database.
 * 
 * @author Deep Risal
 * 
 */
public class DBAdapter {

	private static final String LOG_TAG = DBAdapter.class.getCanonicalName();

	private static final String DATABASE_NAME = MetaSchema.DATABASE_NAME;

	// ST:databaseTableVariableDeclaration:start
	static final String DATABASE_TABLE_EVENT = MetaSchema.Event.TABLE_NAME;
	static final String DATABASE_TABLE_USER = MetaSchema.Users.TABLE_NAME;
	static final String DATABASE_TABLE_EVENT_USER = MetaSchema.Event_Users.TABLE_NAME;
	// ST:databaseTableVariableDeclaration:finish

	static final int DATABASE_VERSION = MetaSchema.DATABASE_VERSION;

	// The SHORT name of each column in your table
	// ST:createShortVariables:start
	private static final String Event_KEY_ID = MetaSchema.Event.Cols.ID;
	private static final String Event_TITLE = MetaSchema.Event.Cols.TITLE;
	private static final String Event_DESCRIPTION = MetaSchema.Event.Cols.DESCRIPTION;
	private static final String Event_START = MetaSchema.Event.Cols.START_TIME;
	private static final String Event_END = MetaSchema.Event.Cols.END_TIME;
	private static final String Event_CREATE = MetaSchema.Event.Cols.CREATION_TIME;
	private static final String Event_UPDATE = MetaSchema.Event.Cols.UPDATE_TIME;
	private static final String Event_LATITUDE = MetaSchema.Event.Cols.LATITUDE;
	private static final String Event_LONGITUDE = MetaSchema.Event.Cols.LONGITUDE;
	private static final String Event_LOCATION = MetaSchema.Event.Cols.LOCATION;
	private static final String Event_COLOR = MetaSchema.Event.Cols.COLOR;
	private static final String Event_CREATED_BY = MetaSchema.Event.Cols.CREATED_BY;
	private static final String Event_OFF_ACTION = MetaSchema.Event.Cols.OFF_ACTION;

	private static final String Invitie_KEY_ID = MetaSchema.Users.Cols.ID;
	private static final String Invitie_UID = MetaSchema.Users.Cols.UID;
	private static final String Invitie_EMAIL = MetaSchema.Users.Cols.EMAIL_ID;
	private static final String Invitie_PHONE = MetaSchema.Users.Cols.PHONE_NO;
	private static final String Invitie_FIRST_NAME = MetaSchema.Users.Cols.FIRST_NAME;
	private static final String Invitie_LAST_NAME = MetaSchema.Users.Cols.LAST_NAME;
	private static final String Invitie_CREATED_AT = MetaSchema.Users.Cols.CREATED_AT;
	private static final String Invitie_IS_INVITEE = MetaSchema.Users.Cols.IS_INVITEE;
	private static final String Invitie_AUTH_KEY = MetaSchema.Users.Cols.AUTH_KEY;

	private static final String Event_Invitie_KEY_ID = MetaSchema.Event_Users.Cols.ID;
	private static final String Invitie_ID = MetaSchema.Event_Users.Cols.USER_ID;
	private static final String Event_ID = MetaSchema.Event_Users.Cols.EVENT_ID;

	// ST:createShortVariables:finish

	// ST:databaseTableCreationStrings:start
	// SQL Statement to create a new database table.
	private static final String DATABASE_CREATE_EVENT = "create table "
			+ DATABASE_TABLE_EVENT + " (" // start table
			+ Event_KEY_ID + " integer primary key autoincrement, " // setup
																	// auto-inc.
			// ST:tableCreateVariables:start
			+ Event_TITLE + " TEXT ," //
			+ Event_DESCRIPTION + " TEXT ," //
			+ Event_START + " INTEGER ," //
			+ Event_END + " INTEGER ," //
			+ Event_CREATE + " INTEGER ," //
			+ Event_UPDATE + " INTEGER ," //
			+ Event_LATITUDE + " REAL ," //
			+ Event_LONGITUDE + " REAL, " //
			+ Event_LOCATION + " TEXT, " //
			+ Event_COLOR + " INTEGER, " //
			+ Event_CREATED_BY + " INTEGER, " //
			+ Event_OFF_ACTION + " TEXT " //
			// ST:tableCreateVariables:finish
			+ " );"; // end table
	// SQL Statement to create a new database table.
	private static final String DATABASE_CREATE_EVENT_USERS = "create table "
			+ DATABASE_TABLE_EVENT_USER + " (" // start table
			+ Event_Invitie_KEY_ID + " integer primary key autoincrement, " // setup
			// auto-inc.
			// ST:tableCreateVariables:start
			+ Invitie_ID + " INTEGER ," //
			+ Event_ID + " INTEGER " //
			// ST:tableCreateVariables:finish
			+ " );"; // end table
	// ST:databaseTableCreationStrings:finish

	// SQL Statement to create a new database table.
	private static final String DATABASE_CREATE_USERS = "create table "
			+ DATABASE_TABLE_USER + " (" // start table
			+ Invitie_KEY_ID + " integer primary key autoincrement, " // setup
																		// auto-inc.
			// ST:tableCreateVariables:start
			+ Invitie_UID + " TEXT , " //
			+ Invitie_EMAIL + " TEXT UINQUE," //
			+ Invitie_PHONE + " TEXT ," //
			+ Invitie_FIRST_NAME + " TEXT , " //
			+ Invitie_LAST_NAME + " TEXT , " //
			+ Invitie_CREATED_AT + " TEXT, " //
			+ Invitie_IS_INVITEE + " TEXT,  " //
			+ Invitie_AUTH_KEY + " TEXT  " //
			// ST:tableCreateVariables:finish
			+ " );"; // end table
	// ST:databaseTableCreationStrings:finish

	// Variable to hold the database instance.
	private SQLiteDatabase db;
	// Context of the application using the database.
	private final Context context;
	// Database open/upgrade helper
	private DbHelper dbHelper;
	// if the DB is in memory or to file.
	private boolean MEMORY_ONLY_DB = false;

	/**
	 * constructor that accepts the context to be associated with
	 * 
	 * @param _context
	 */
	public DBAdapter(Context _context) {
		Log.d(LOG_TAG, "MyDBAdapter constructor");

		context = _context;
		dbHelper = new DbHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	/**
	 * constructor that accepts the context to be associated with, and if this
	 * DB should be created in memory only(non-persistent).
	 * 
	 * @param _context
	 */
	public DBAdapter(Context _context, boolean memory_only_db) {
		Log.d(LOG_TAG, "MyDBAdapter constructor w/ mem only =" + memory_only_db);

		context = _context;
		MEMORY_ONLY_DB = memory_only_db;
		if (MEMORY_ONLY_DB == true) {
			dbHelper = new DbHelper(context, null, null, DATABASE_VERSION);
		} else {
			dbHelper = new DbHelper(context, DATABASE_NAME, null,
					DATABASE_VERSION);
		}
	}

	/**
	 * open the DB Get Memory or File version of DB, and write/read access or
	 * just read access if that is all that is possible.
	 * 
	 * @return this MoocDataDBAdaptor
	 * @throws SQLException
	 */
	public DBAdapter open() throws SQLException {
		Log.d(LOG_TAG, "open()");
		try {
			db = dbHelper.getWritableDatabase();
		} catch (SQLException ex) {
			db = dbHelper.getReadableDatabase();
		}
		return this;
	}

	/**
	 * Remove a row of the DB where the rowIndex matches.
	 * 
	 * @param rowIndex
	 *            row to remove from DB
	 * @return if the row was removed
	 */
	public int delete(final String table, long _id) {
		Log.d(LOG_TAG, "delete(" + _id + ") ");
		return db.delete(table, android.provider.BaseColumns._ID + " = " + _id,
				null);
	}

	/**
	 * Delete row(s) that match the whereClause and whereArgs(if used).
	 * <p>
	 * the whereArgs is an String[] of values to substitute for the '?'s in the
	 * whereClause
	 * 
	 * @param whereClause
	 * @param whereArgs
	 * @return
	 */
	public int delete(final String table, final String whereClause,
			final String[] whereArgs) {
		Log.d(LOG_TAG, "delete(" + whereClause + ") ");
		return db.delete(table, whereClause, whereArgs);
	}

	/**
	 * Query the Database with the provided specifics.
	 * 
	 * @param projection
	 * @param selection
	 * @param selectionArgs
	 * @param sortOrder
	 * @return Cursor of results
	 */
	public Cursor query(final String table, final String[] projection,
			final String selection, final String[] selectionArgs,
			final String sortOrder) {

		// Perform a query on the database with the given parameters
		return db.query(table, projection, selection, selectionArgs, null,
				null, sortOrder);
	}

	/**
	 * close the DB.
	 */
	public void close() {
		Log.d(LOG_TAG, "close()");
		db.close();
	}

	/**
	 * Start a transaction.
	 */
	public void startTransaction() {
		Log.d(LOG_TAG, "startTransaction()");
		db.beginTransaction();
	}

	/**
	 * End a transaction.
	 */
	public void endTransaction() {
		Log.d(LOG_TAG, "endTransaction()");
		db.endTransaction();
	}

	/**
	 * Get the underlying Database.
	 * 
	 * @return
	 */
	SQLiteDatabase getDB() {
		return db;
	}

	/**
	 * Insert a ContentValues into the DB.
	 * 
	 * @param location
	 * @return row's '_id' of the newly inserted ContentValues
	 */
	public long insert(final String table, final ContentValues cv) {
		Log.d(LOG_TAG, "insert(CV)");
		return db.insertOrThrow(table, null, cv);
	}

	/**
	 * Update Value(s) in the DB.
	 * 
	 * @param values
	 * @param whereClause
	 * @param whereArgs
	 * @return number of rows changed.
	 */
	public int update(final String table, final ContentValues values,
			final String whereClause, final String[] whereArgs) {
		return db.update(table, values, whereClause, whereArgs);
	}

	@Override
	/**
	 * finalize operations to this DB, and close it.
	 */
	protected void finalize() throws Throwable {
		try {
			db.close();
		} catch (Exception e) {
			Log.d(LOG_TAG, "exception on finalize():" + e.getMessage());
		}
		super.finalize();
	}

	/**
	 * This class can support running the database in a non-persistent mode,
	 * this tells you if that is happening.
	 * 
	 * @return boolean true/false of if this DBAdaptor is persistent or in
	 *         memory only.
	 */
	public boolean isMemoryOnlyDB() {
		return MEMORY_ONLY_DB;
	}

	public void listDatabaseColumns(String tableName) {
		Cursor dbCursor = db.query(tableName, null, null, null, null, null,
				null);
		String[] columnNames = dbCursor.getColumnNames();
		String col = "";
		for (int i = 0; i < columnNames.length; i++)
			col += columnNames[i] + "\n ";
		Log.i(LOG_TAG, "column: " + columnNames.length + col);
	}

	/**
	 * DB Helper Class.
	 * 
	 * @author Deep Risal
	 * 
	 */
	private static class DbHelper extends SQLiteOpenHelper {

		public DbHelper(Context context, String name, CursorFactory factory,
				int version) {
			super(context, name, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			Log.d(LOG_TAG, "DATABASE_CREATE: version: " + DATABASE_VERSION);
			// ST:createTable:start
			db.execSQL(DATABASE_CREATE_EVENT);
			db.execSQL(DATABASE_CREATE_USERS);
			db.execSQL(DATABASE_CREATE_EVENT_USERS);
			// ST:createTable:finish
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// Log version upgrade.
			Log.w(LOG_TAG + "DBHelper", "Upgrading from version " + oldVersion
					+ " to " + newVersion + ", which will destroy all old data");

			// **** Upgrade DB ****
			// drop old DB

			// ST:dropTableIfExists:start
			db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_EVENT);
			db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_USER);
			db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_EVENT_USER);
			// ST:dropTableIfExists:finish

			// Create a new one.
			onCreate(db);

		}

	}

	// ------------------------ "event's" table methods ----------------//

	/**
	 * Creating a event
	 */
	public long createEvent(Event event) {
		ContentValues cv = EventCreator.getCVfromEvent(event);
		open();
		startTransaction();
		long event_id = 0;
		try {
			// insert row
			event_id = insert(DATABASE_TABLE_EVENT, cv);
			// insert tag_ids

			if (event.getUsersList() != null)
				for (User usr : event.getUsersList()) {
					ContentValues values = new ContentValues();
					values.put(MetaSchema.Event_Users.Cols.EVENT_ID, event_id);
					values.put(MetaSchema.Event_Users.Cols.USER_ID,
							usr.getUserId());
					insert(DATABASE_TABLE_EVENT_USER, values);
				}
			db.setTransactionSuccessful();
		} catch (SQLiteException ex) {
			endTransaction();
		} finally {
			endTransaction();
			close();
		}

		return event_id;
	}

	/**
	 * get single event
	 */
	public Event getEvent(long event_id) {
		String selectQuery = "SELECT  * FROM " + DATABASE_TABLE_EVENT
				+ " WHERE " + BaseColumns._ID + " = " + event_id;
		open();
		Log.i(LOG_TAG, selectQuery);
		Cursor c = db.rawQuery(selectQuery, null);
		Event event = EventCreator.getEventDataFromCursor(c);
		c.close();
		close();
		return event;
	}

	/**
	 * Query for each Event
	 * 
	 * @param projection
	 * @param selection
	 * @param selectionArgs
	 * @param sortOrder
	 * @return an ArrayList of Event objects
	 */
	public Event getEventData(final String[] projection,
			final String selection, final String[] selectionArgs,
			final String sortOrder) {
		open();
		// query the C.P.
		Cursor result = query(DATABASE_TABLE_EVENT, projection, selection,
				selectionArgs, sortOrder);
		// make return object
		Event rValue = EventCreator.getEventDataFromCursor(result);
		// convert cursor to reutrn object
		result.close();
		close();
		// return 'return object'
		return rValue;
	}

	/**
	 * Query for each Event
	 * 
	 * @param projection
	 * @param selection
	 * @param selectionArgs
	 * @param sortOrder
	 * @return an ArrayList of Event objects
	 */
	public ArrayList<Event> queryEventData(final String[] projection,
			final String selection, final String[] selectionArgs,
			final String sortOrder) {
		open();
		// query the C.P.
		Cursor result = query(DATABASE_TABLE_EVENT, projection, selection,
				selectionArgs, sortOrder);
		// make return object
		ArrayList<Event> rValue = new ArrayList<Event>();
		// convert cursor to reutrn object
		rValue.addAll(EventCreator.getEventDataArrayListFromCursor(result));
		result.close();
		close();
		// return 'return object'
		return rValue;
	}

	public boolean deleteAllEvents() {
		boolean isDelete=false;
		open();
		startTransaction();
		try {
			db.delete(DATABASE_TABLE_EVENT, MetaSchema.Event.Cols.OFF_ACTION
					+ "= ?", new String[] { "N" });
			db.delete(DATABASE_TABLE_EVENT_USER, null, null);
			db.setTransactionSuccessful();
		} catch (SQLiteException ex) {
			endTransaction();
			isDelete=false;
		} finally {
			isDelete=true;
			endTransaction();
			close();
			
		}
		return isDelete;

	}

	public int updateEvent(final Event eventData, final String whereClause,
			final String[] whereArgs) {
		int updatedNumb = 0;
		Long event_id = eventData.getId();
		open();
		startTransaction();
		try {
			ContentValues values = EventCreator.getCVfromEvent(eventData);
			updatedNumb = db.update(DATABASE_TABLE_EVENT, values, whereClause,
					whereArgs);
			// Delete Previous added event user relationship
			db.delete(DATABASE_TABLE_EVENT_USER,
					MetaSchema.Event_Users.Cols.EVENT_ID + "= ?",
					new String[] { event_id.toString() });

			// Added new event user relationship
			if (eventData.getUsersList() != null)
				for (User usr : eventData.getUsersList()) {
					ContentValues value = new ContentValues();
					value.put(MetaSchema.Event_Users.Cols.EVENT_ID, event_id);
					value.put(MetaSchema.Event_Users.Cols.USER_ID,
							usr.getUserId());
					insert(DATABASE_TABLE_EVENT_USER, value);
				}
			db.setTransactionSuccessful();
		} catch (SQLiteException ex) {
			endTransaction();
		} finally {
			endTransaction();
			close();
		}
		return updatedNumb;
	}

	/**
	 * getting all events
	 * */
	@SuppressWarnings("unchecked")
	public List<Event> getAllEvents() {
		List<Event> events = new ArrayList<Event>();
		List<User> Users = new ArrayList<User>();
		String selectQuery = "SELECT  * FROM " + DATABASE_TABLE_EVENT;
		Log.i(LOG_TAG, selectQuery);
		open();
		Cursor c = db.rawQuery(selectQuery, null);
		// looping through all rows and adding to list
		events = EventCreator.getEventDataArrayListFromCursor(c);
		for (Event e : events) {
			Users = (List<User>) getEventsWithAllInvities(e.getId());
			e.setUsersList(Users);
		}
		close();

		return events;
	}

	/**
	 * Deleting a event
	 */
	public void deleteEvent(Long event_id) {
		Cursor cursor = null;
		open();
		startTransaction();
		// ArrayList<Users> UsersList = new ArrayList<Users>();
		// delete all link event_Users of Users list
		try {
			Log.i(LOG_TAG, "Delete Event of id: "+ event_id);
			// cursor = db.rawQuery(selectQuery, null);
			cursor = query(DATABASE_TABLE_EVENT_USER,
					new String[] { BaseColumns._ID },
					MetaSchema.Event_Users.Cols.EVENT_ID + "= ?",
					new String[] { event_id.toString() }, null);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						db.delete(DATABASE_TABLE_EVENT_USER, BaseColumns._ID
								+ " = ?", new String[] { String.valueOf(cursor
								.getColumnIndex(BaseColumns._ID)) });
					} while (cursor.moveToNext() == true);
				}
			}
			// now delete the event
			db.delete(DATABASE_TABLE_EVENT, BaseColumns._ID + " = ?",
					new String[] { String.valueOf(event_id) });
			db.setTransactionSuccessful();

		} catch (SQLiteException ex) {
			cursor.close();
			endTransaction();
		} finally {
			cursor.close();
			endTransaction();
			close();
		}
	}

	/**
	 * getting event count
	 */
	public int getEventCount() {
		String countQuery = "SELECT  * FROM " + DATABASE_TABLE_EVENT;
		open();
		Cursor cursor = db.rawQuery(countQuery, null);
		int count = cursor.getCount();
		cursor.close();
		close();
		// return count
		return count;

	}

	// ------------------------ "invitees" table methods ----------------//

	/**
	 * Creating Users
	 */
	public long createUser(User invitee) {
		ContentValues cv = UserCreator.getCVfromInvitee(invitee);
		long invitee_id = 0;
		open();
		startTransaction();
		try {
			listDatabaseColumns(DATABASE_TABLE_USER);
			// insert row
			invitee_id = insert(DATABASE_TABLE_USER, cv);
			db.setTransactionSuccessful();
		} catch (SQLiteException ex) {
			endTransaction();
		} finally {
			endTransaction();
			close();
		}
		return invitee_id;
	}

	/**
	 * Creating Users
	 */
	public long getUserId(String authKey) {
		String selectQuery = "SELECT * FROM " + DATABASE_TABLE_USER;
		/*
		 * " WHERE " + MetaSchema.Users.Cols.AUTH_KEY + " =\'" + authKey + "\'";
		 */
		String[] selectionArgs = new String[1];
		selectionArgs[0] = authKey;
		long userId = -1;
		open();
		Log.i(LOG_TAG, selectQuery);
		// Cursor c = db.rawQuery(selectQuery, null);
		Cursor c = query(DATABASE_TABLE_USER,
				new String[] { MetaSchema.Users.Cols.ID },
				MetaSchema.Users.Cols.AUTH_KEY + "=?", selectionArgs, null);

		if (c != null && c.moveToNext()) {
			userId = c.getLong(0);
		}

		close();
		return userId;

	}

	/**
	 * getting all Users
	 * */
	public List<User> getAllUsers() {
		List<User> usersList = new ArrayList<User>();
		String selectQuery = "SELECT  * FROM " + DATABASE_TABLE_USER;
		Log.e(LOG_TAG, selectQuery);
		open();
		Cursor c = db.rawQuery(selectQuery, null);
		usersList = UserCreator.getInviteeDataArrayListFromCursor(c);
		close();
		return usersList;
	}

	/**
	 * get single Users
	 */
	public User getInvites(long invitee_id) {
		String selectQuery = "SELECT  * FROM " + DATABASE_TABLE_USER
				+ " WHERE " + BaseColumns._ID + " = " + invitee_id;
		open();
		Log.i(LOG_TAG, selectQuery);
		Cursor c = db.rawQuery(selectQuery, null);
		close();
		return UserCreator.getUsersDataFromCursor(c);
	}

	/**
	 * getting Users count
	 */
	public int getUsersCount() {
		String countQuery = "SELECT  * FROM " + DATABASE_TABLE_USER;
		open();
		Cursor cursor = db.rawQuery(countQuery, null);
		int count = cursor.getCount();
		cursor.close();
		// return count
		return count;
	}
	
	
	public boolean deleteAllUsers() {
		boolean isDelete=false;
		open();
		startTransaction();
		try {			
			db.delete(DATABASE_TABLE_USER, null, null);
			db.setTransactionSuccessful();
		} catch (SQLiteException ex) {
			endTransaction();
			isDelete=false;
		} finally {
			isDelete=true;
			endTransaction();
			close();			
		}
		return isDelete;

	}

	// ------------------------ "event_invitees" table methods
	// ----------------//

	/**
	 * Creating event_invitees
	 */
	public long createEventUsers(long event_id, long user_id) {
		long id = 0;
		open();
		startTransaction();
		ContentValues values = new ContentValues();
		values.put(MetaSchema.Event_Users.Cols.EVENT_ID, event_id);
		values.put(MetaSchema.Event_Users.Cols.USER_ID, user_id);
		try {
			id = insert(DATABASE_TABLE_EVENT_USER, values);
			db.setTransactionSuccessful();
		} catch (SQLiteException ex) {
			endTransaction();
		} finally {
			endTransaction();
			close();
		}
		return id;
	}

	/**
	 * Query for each Event
	 * 
	 * @param projection
	 * @param selection
	 * @param selectionArgs
	 * @param sortOrder
	 * @return an ArrayList of Event objects
	 */
	public ArrayList<User> queryUsersData(final String[] projection,
			final String selection, final String[] selectionArgs,
			final String sortOrder) {
		open();
		// query the C.P.
		Cursor result = query(DATABASE_TABLE_USER, projection, selection,
				selectionArgs, sortOrder);
		// make return object
		ArrayList<User> rValue = new ArrayList<User>();
		// convert cursor to reutrn object
		rValue.addAll(UserCreator.getInviteeDataArrayListFromCursor(result));
		result.close();
		close();
		// return 'return object'
		return rValue;
	}

	/**
	 * get event with all invities
	 */
	public Event getEventsWithAllInvities(Long event_id) {
		ArrayList<User> usersList = new ArrayList<User>();
		Event event = (queryEventData(null, MetaSchema.Event.Cols.ID + " = ? ",
				new String[] { event_id.toString() }, null)).get(0);
		open();
		Log.i(LOG_TAG, "Fetching Event with all Invitees ");
		// Cursor cursor = db.rawQuery(selectQuery, null);
		Cursor cursor = query(DATABASE_TABLE_EVENT_USER,
				new String[] { MetaSchema.Event_Users.Cols.USER_ID },
				MetaSchema.Event_Users.Cols.EVENT_ID + "= ?",
				new String[] { event_id.toString() }, null);
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {
					Long userId = cursor
							.getLong(cursor
									.getColumnIndex(MetaSchema.Event_Users.Cols.USER_ID));
					Cursor cur = (query(DATABASE_TABLE_USER, null,
							BaseColumns._ID + "= ?",
							new String[] { userId.toString() }, null));
					ArrayList<User> rValue = new ArrayList<User>();
					// convert cursor to reutrn object
					rValue.addAll(UserCreator
							.getInviteeDataArrayListFromCursor(cur));
					usersList.add(rValue.get(0));
				} while (cursor.moveToNext() == true);
			}
		}
		event.setUsersList(usersList);
		close();
		return event;

	}

	// ------------------------ "user" table methods ----------------//

	/**
	 * get datetime
	 * */
	private String getDateTime(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss", Locale.getDefault());
		return dateFormat.format(date);
	}

}
