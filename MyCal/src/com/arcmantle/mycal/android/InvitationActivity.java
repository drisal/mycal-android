package com.arcmantle.mycal.android;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.arcmantle.mycal.android.customview.DateTimePicker;
import com.arcmantle.mycal.android.event.DateWatcher;
import com.arcmantle.mycal.android.googlemaps.MapActivity;
import com.arcmantle.mycal.android.orm.Event;
import com.arcmantle.mycal.android.provider.CalendarProvider;
import com.google.android.gms.maps.GoogleMap;


public class InvitationActivity extends Activity  implements DateWatcher {
	
	GoogleMap googleMap;
	EditText startDay, endDay,description,title,location;
	int startDayYear, startDayMonth, startDayDay, startTimeHour, startTimeMin;
	int endDayYear, endDayMonth, endDayDay, endTimeHour, endTimeMin;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_invitation);
		title=(EditText) findViewById(R.id.titleText);
		description=(EditText) findViewById(R.id.descriptionText);		
		startDay = (EditText) findViewById(R.id.startDayTime);
		endDay=(EditText) findViewById(R.id.endDayTime);
		//initilizeMap();
	}
	
	
	
	public void onClickStartDayButton(View view){
		 button_click(view,startDay);
	}
	
	public void onClickEndDayButton(View view){
		 button_click(view,endDay);
	}
	
	public void button_click(View view,final EditText edit_text){  
		// Create the dialog
    	final Dialog mDateTimeDialog = new Dialog(this);
		// Inflate the root layout
		final RelativeLayout mDateTimeDialogView = (RelativeLayout) getLayoutInflater().inflate(R.layout.date_time_dialog, null);
		// Grab widget instance
		final DateTimePicker mDateTimePicker = (DateTimePicker) mDateTimeDialogView.findViewById(R.id.DateTimePicker);
		mDateTimePicker.setDateChangedListener(this); 
		 
		// Update demo TextViews when the "OK" button is clicked 
		((Button) mDateTimeDialogView.findViewById(R.id.SetDateTime)).setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				mDateTimePicker.clearFocus();
				// TODO Auto-generated method stub 
				String result_string = mDateTimePicker.getMonth() + "/" + String.valueOf(mDateTimePicker.getDay()) + "/" + String.valueOf(mDateTimePicker.getYear())
						+ "  " + String.valueOf(mDateTimePicker.getHour()) + ":" + String.valueOf(mDateTimePicker.getMinute());
//				if(mDateTimePicker.getHour() > 12) result_string = result_string + "PM";
//				else result_string = result_string + "AM";
				edit_text.setText(result_string);
				mDateTimeDialog.dismiss();
			}
		});

		// Cancel the dialog when the "Cancel" button is clicked
		((Button) mDateTimeDialogView.findViewById(R.id.CancelDialog)).setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				mDateTimeDialog.cancel();
			}
		});

		// Reset Date and Time pickers when the "Reset" button is clicked
	
		((Button) mDateTimeDialogView.findViewById(R.id.ResetDateTime)).setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				mDateTimePicker.reset();
			}
		});
		  
		// Setup TimePicker
		// No title on the dialog window
		mDateTimeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// Set the dialog content view
		mDateTimeDialog.setContentView(mDateTimeDialogView);
		// Display the dialog
		mDateTimeDialog.show();				
    }
	public void onDateChanged(Calendar c) { 
		Log.i("Date Changed:",
				"" + c.get(Calendar.MONTH) + " " + c.get(Calendar.DAY_OF_MONTH)
						+ " " + c.get(Calendar.YEAR)+ " " + c.get(Calendar.HOUR_OF_DAY)+ " " + c.get(Calendar.MINUTE));
	}

	
	public void onCreateEvent(View view){
		ContentValues values = new ContentValues();
		values.put(CalendarProvider.COLOR, Event.COLOR_RED);
		values.put(CalendarProvider.DESCRIPTION,description.toString() );
		values.put(CalendarProvider.LOCATION, "Some location");
		values.put(CalendarProvider.EVENT, title.toString());
			
		Calendar cal = Calendar.getInstance();
		TimeZone tz = TimeZone.getDefault();	
		cal.set(startDayYear, startDayMonth, startDayDay, startTimeHour, startTimeMin);
		int startDayJulian = Time.getJulianDay(cal.getTimeInMillis(), TimeUnit.MILLISECONDS.toSeconds(tz.getOffset(cal.getTimeInMillis())));
		values.put(CalendarProvider.START, cal.getTimeInMillis());
		values.put(CalendarProvider.START_DAY, startDayJulian);
		
			
		cal.set(endDayYear, endDayMonth, endDayDay, endTimeHour, endTimeMin);
		int endDayJulian = Time.getJulianDay(cal.getTimeInMillis(), TimeUnit.MILLISECONDS.toSeconds(tz.getOffset(cal.getTimeInMillis())));
			
		values.put(CalendarProvider.END, cal.getTimeInMillis());
		values.put(CalendarProvider.END_DAY, endDayJulian);

		Uri uri = getContentResolver().insert(CalendarProvider.CONTENT_URI, values);
		
		
		Log.i("Create Event", "Successfully Create Event");
		
		
	}
	
	

	
}
