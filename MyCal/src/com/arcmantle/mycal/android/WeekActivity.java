package com.arcmantle.mycal.android;

import java.util.Calendar;
import java.util.Date;

import org.joda.time.DateTime;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class WeekActivity extends FragmentActivity {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments representing each object in a collection. We use a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter} derivative,
	 * which will destroy and re-create fragments as needed, saving and
	 * restoring their state in the process. This is important to conserve
	 * memory and is a best practice when allowing navigation between objects in
	 * a potentially large collection.
	 */
	WeekPagerAdapter mWeekPagerAdapter;

	/**
	 * The {@link android.support.v4.view.ViewPager} that will display the
	 * object collection.
	 */
	ViewPager mViewPager;

	static Integer CAL_START_YEAR = 1970;
	static Integer CAL_END_YEAR = 2250;
	static Integer WEEK_NUM;
	static Integer YEAR;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_week);

		// Create an adapter that when requested, will return a fragment
		// representing an object in
		// the collection.
		//
		// ViewPager and its adapters use support library fragments, so we must
		// use
		// getSupportFragmentManager.
		mWeekPagerAdapter = new WeekPagerAdapter(getSupportFragmentManager());

		// Set up action bar.
		// final ActionBar actionBar = getActionBar();

		// Specify that the Home button should show an "Up" caret, indicating
		// that touching the
		// button will take the user one step up in the application's hierarchy.
		// actionBar.setDisplayHomeAsUpEnabled(true);

		// Set up the ViewPager, attaching the adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mWeekPagerAdapter);
		mViewPager.setCurrentItem(getCurrentPosition());
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This is called when the Home (Up) button is pressed in the action
			// bar.
			// Create a simple intent that starts the hierarchical parent
			// activity and
			// use NavUtils in the Support Package to ensure proper handling of
			// Up.
			Intent upIntent = new Intent(this, MyCalMainActivity.class);
			if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
				// This activity is not part of the application's task, so
				// create a new task
				// with a synthesized back stack.
				TaskStackBuilder.from(this)
				// If there are ancestor activities, they should be added here.
						.addNextIntent(upIntent).startActivities();
				finish();
			} else {
				// This activity is part of the application's task, so simply
				// navigate up to the hierarchical parent activity.
				NavUtils.navigateUpTo(this, upIntent);
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A {@link android.support.v4.app.FragmentStatePagerAdapter} that returns a
	 * fragment representing an object in the collection.
	 */
	public static class WeekPagerAdapter extends FragmentStatePagerAdapter {

		public WeekPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int i) {
			Fragment fragment = new WeekFragment();
			Bundle args = new Bundle();
			args.putInt(WeekFragment.WEEK_NUM, WEEK_NUM);
			args.putInt(WeekFragment.YEAR, YEAR);// Our object is just
			// an integer :-P
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			// For this contrived example, we have a 100-object collection.
			return (CAL_END_YEAR - CAL_START_YEAR) * 52;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			YEAR = getYear(position);
			WEEK_NUM = position - (YEAR - CAL_START_YEAR) * 52 + 52;
			return "WEEK " + WEEK_NUM;
		}

	}

	/**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	public static class WeekFragment extends Fragment {

		public static final String WEEK_NUM = "week";
		public static final String YEAR = "year";

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.week_view, container,
					false);
			String weekDays[]=new String[7];
			Bundle args = getArguments();
			((TextView) rootView.findViewById(R.id.currentYearTextView))
					.setText(Integer.toString(args.getInt(YEAR)));
			weekDays=getWeekDays(args.getInt(WEEK_NUM),args.getInt(YEAR));
			((TextView) rootView.findViewById(R.id.sundayDateTextView))
			.setText(weekDays[0]);
			((TextView) rootView.findViewById(R.id.mondayDateTextView))
			.setText(weekDays[1]);
			((TextView) rootView.findViewById(R.id.tuesdayDateTextView))
			.setText(weekDays[2]);
			((TextView) rootView.findViewById(R.id.wednesdayDateTextView))
			.setText(weekDays[3]);
			((TextView) rootView.findViewById(R.id.thursdayDateTextView))
			.setText(weekDays[4]);
			((TextView) rootView.findViewById(R.id.fridayDateTextView))
			.setText(weekDays[5]);
			((TextView) rootView.findViewById(R.id.saturdayDateTextView))
			.setText(weekDays[6]);
			
			return rootView;
		}

	}

	public static Integer getWeekNumber() {
		DateTime newDate = new DateTime();
		return newDate.getWeekOfWeekyear();

	}

	public static Integer getCurrentYear() {
		DateTime newDate = new DateTime();
		return newDate.getWeekyear();

	}

	public static Integer getCurrentPosition() {
		return (getCurrentYear() - CAL_START_YEAR) * 52
				- (52 - getWeekNumber());
	}

	public static Integer getYear(Integer position) {
		if (position % 52 == 0)
			return position / 52 + CAL_START_YEAR;
		else
			return position / 52 + 1 + CAL_START_YEAR;

	}

	@SuppressWarnings("deprecation")
	public static String[] getWeekDays(int week, int year){
		// Get calendar, clear it and set week number and year.
		String[] weekDates=new String[7];
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.WEEK_OF_YEAR, week);
		calendar.set(Calendar.YEAR, year);

		// Now get the first day of week.
		Date date1 = calendar.getTime();		
		calendar.add(Calendar.DAY_OF_YEAR,-1);
		weekDates[0]=(calendar.get(Calendar.MONTH)+1)+"/"+calendar.get(Calendar.DATE);
		for(int i=1;i<7;i++){			
			    calendar.add(Calendar.DAY_OF_YEAR, 1);			  
			    weekDates[i]=(calendar.get(Calendar.MONTH)+1)+"/"+calendar.get(Calendar.DATE);
		}
		
		return weekDates;
	}
}
