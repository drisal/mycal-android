package com.arcmantle.mycal.android.event;

import com.arcmantle.mycal.android.R;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

/**
 * Fragments require a Container Activity, this is the one for the Invite Event
 * Activity.
 */
public class InviteEventActivity extends EventActivityBase {

	private final static String LOG_TAG = InviteEventActivity.class
			.getCanonicalName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(LOG_TAG, "onCreate");
		setContentView(R.layout.activity_invitation);
		InviteEventFragment fragment;
		String imageFragmentTag = "imageFragmentTag";
		if (savedInstanceState == null) {
			fragment = new InviteEventFragment();
			getSupportFragmentManager().beginTransaction()
					.add(R.id.locations, fragment, imageFragmentTag).commit();
		}

	}

	@Override
	public boolean onKeyDown(final int keyCode, final KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			event.startTracking();
			return true;
		}
		return super.onKeyLongPress(keyCode, event);
	}

	@Override
	public boolean onKeyLongPress(final int keyCode, final KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
			return true;
		}
		return super.onKeyLongPress(keyCode, event);
	}

}
