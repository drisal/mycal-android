package com.arcmantle.mycal.android.event;


public interface OnOpenWindowInterface {
	
	public void openEditEventFragment(long index);
	
	public void openCreateEventFragment();

	public void openListEventFragment();
	
	public void openViewEventFragment(long index);
	
	public void inviteEventFragment(Object[] inviteeList);
}
