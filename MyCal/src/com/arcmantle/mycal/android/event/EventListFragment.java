package com.arcmantle.mycal.android.event;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.arcmantle.mycal.android.R;
import com.arcmantle.mycal.android.orm.Event;
import com.arcmantle.mycal.android.provider.DBAdapter;
import com.arcmantle.mycal.android.provider.MetaSchema;

/**
 * Fragment to hold all the UI components and related Logic for Listing Event.
 * 
 * @author Deep Risal
 * 
 */
public class EventListFragment extends ListFragment {

	static final String LOG_TAG = EventListFragment.class.getCanonicalName();

	OnOpenWindowInterface mOpener;
	DBAdapter adapter;
	ArrayList<Event> eventList;
	private EventArrayAdaptor aa;

	EditText filterET;
	//ProgressDialog getServerData;

	/**
	 * @see android.support.v4.app.Fragment#onAttach(android.app.Activity)
	 */
	@Override
	public void onAttach(Activity activity) {
		Log.d(LOG_TAG, "onAttach start");
		super.onAttach(activity);
		try {
			mOpener = (OnOpenWindowInterface) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnOpenWindowListener" + e.getMessage());
		}
		Log.d(LOG_TAG, "onAttach end");
	}

	@Override
	/**
	 * @see android.support.v4.app.Fragment#onDetach()
	 */
	public void onDetach() {
		super.onDetach();
		mOpener = null;
	}

	/**
	 * The system calls this when creating the fragment. Within your
	 * implementation, you should initialize essential components of the
	 * fragment that you want to retain when the fragment is paused or stopped,
	 * then resumed.
	 */
	@Override
	/**
	 * @see android.support.v4.app.Fragment#onCreate(android.os.Bundle)
	 */
	public void onCreate(Bundle savedInstanceState) {
		Log.d(LOG_TAG, "onCreate");
		super.onCreate(savedInstanceState);
		adapter = new DBAdapter(getActivity());
		eventList = new ArrayList<Event>();
		setRetainInstance(true);
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.app.ListFragment#onCreateView(android.view.LayoutInflater
	 * , android.view.ViewGroup, android.os.Bundle)
	 */
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.event_listview, container, false);
		// get the ListView that will be displayed
		ListView lv = (ListView) view.findViewById(android.R.id.list);
		TextView emptyText = (TextView)view.findViewById(android.R.id.empty);
		lv.setEmptyView(emptyText);
		filterET = (EditText) view
				.findViewById(R.id.event_listview_tags_filter);

		filterET.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				updateEventData();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});

		// customize the ListView in whatever desired ways.
		lv.setBackgroundColor(Color.GRAY);
		// return the parent view
		return view;
	}

	//
	// This function is called every time the filter EditText is changed
	// This function should update the ListView to match the specified
	// filter text.
	//

	public void updateEventData() {
		Log.d(LOG_TAG, "updateStoryData");
		try {
			eventList.clear();

			String filterWord = filterET.getText().toString();
			adapter.getEventCount();
			// create String that will match with 'like' in query
			filterWord = "%" + filterWord + "%";

			ArrayList<Event> currentList2 = adapter.queryEventData(null,
					MetaSchema.Event.Cols.TITLE + " LIKE ? ",
					new String[] { filterWord }, MetaSchema.Event.Cols.ID +" DESC");

			eventList.addAll(currentList2);
			aa.notifyDataSetChanged();
		} catch (Exception e) {
			Log.e(LOG_TAG,
					"Error connecting to Content Provider" + e.getMessage());
			e.printStackTrace();
		}

	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onActivityCreated(android.os.Bundle)
	 */
	public void onActivityCreated(Bundle savedInstanceState) {
		// create the custom array adapter that will make the custom row
		// layouts
		super.onActivityCreated(savedInstanceState);
		Log.d(LOG_TAG, "onActivityCreated");
		aa = new EventArrayAdaptor(getActivity(),
				R.layout.event_listview_custom_row, eventList);

		// update the back end data.
		updateEventData();

		setListAdapter(aa);

		ImageButton createNewButton = (ImageButton) getView().findViewById(
				R.id.event_list_create);
		createNewButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mOpener.openCreateEventFragment();
			}
		});
	}

	/*
	 * Refresh story list on fragment resume (rather than having to manually
	 * click a refresh button) (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		super.onResume();
		updateEventData();
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.app.ListFragment#onListItemClick(android.widget.ListView
	 * , android.view.View, int, long)
	 */
	public void onListItemClick(ListView l, View v, int position, long id) {
		Log.d(LOG_TAG, "onListItemClick");
		Log.d(LOG_TAG,
				"position: " + position + "id = "
						+ (eventList.get(position)).getId());
		mOpener.openViewEventFragment((eventList.get(position)).getId());
	}
}
