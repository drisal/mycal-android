package com.arcmantle.mycal.android.event;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

import com.arcmantle.mycal.android.R;



/**
 * Fragments require a Container Activity, this is the one for the List
 * Event
 */
public class EventListActivity extends EventActivityBase {
    private static final String LOG_TAG = EventListActivity.class
            .getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(LOG_TAG, "onCreate");
        promptOnBackPressed = true;
        // set the Layout of main Activity.
        // (contains only the fragment holder)
        setContentView(R.layout.activity_invitation);
        EventListFragment fragment;
        String imageFragmentTag = "imageFragmentTag";
        if (savedInstanceState == null) {
            fragment = new EventListFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.locations, fragment, imageFragmentTag).commit();
        }

    }

	@Override
    public boolean onKeyDown(final int keyCode, final KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            event.startTracking();
            return true;
        }
        return super.onKeyLongPress(keyCode, event);
    }

    @Override
    public boolean onKeyLongPress(final int keyCode, final KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            return true;
        }
        return super.onKeyLongPress(keyCode, event);
    }
}