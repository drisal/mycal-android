package com.arcmantle.mycal.android.event;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.arcmantle.mycal.android.R;
import com.arcmantle.mycal.android.gcm.GcmConfig;
import com.arcmantle.mycal.android.googlemaps.Place;
import com.arcmantle.mycal.android.orm.Event;
import com.arcmantle.mycal.android.orm.User;
import com.arcmantle.mycal.android.provider.DBAdapter;
import com.arcmantle.mycal.android.provider.MetaSchema;
import com.arcmantle.mycal.android.util.ConnectionDetector;
import com.arcmantle.mycal.android.util.JSONParser;
import com.arcmantle.mycal.android.util.OffAction;

public class EditEventFragment extends Fragment {

	final static public String LOG_TAG = EditEventFragment.class
			.getCanonicalName();
	// variable for passing around row index
	final static public String rowIdentifyerTAG = "index";
	static private final int GET_TEXT_REQUEST_CODE = 1;
	static final String TAG = "UDATE EVENT";
	EditText startDayET, endDayET, descriptionET, titleET, locationET;
	Date startDate, endDate;
	ImageButton pickLocationBtn;
	String createDate;
	TextView latitudeValue;
	TextView inviteeAdd;
	RelativeLayout addPeople;
	TextView longitudeValue;
	Location loc;
	List<User> selectedUser;
	Place place;
	ConnectionDetector connectionDetector;
	// Button(s) used
	Button startTime;
	Button endTime;
	Button buttonUpdate;
	Button buttonClear;
	Button buttonCancel;

	RequestQueue queue;
	// parent Activity
	OnOpenWindowInterface mOpener;
	// custom ContentResolver wrapper.
	DBAdapter adapter;

	// listener to button presses.
	OnClickListener myOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {

		}
	};

	public static EditEventFragment newInstance(long index) {
		EditEventFragment f = new EditEventFragment();
		// Supply index input as an argument.
		Bundle args = new Bundle();
		args.putLong(rowIdentifyerTAG, index);
		f.setArguments(args);
		return f;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mOpener = (OnOpenWindowInterface) activity;
			adapter = new DBAdapter(activity);
			connectionDetector = new ConnectionDetector(
					activity.getApplicationContext());
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnOpenWindowListener");
		}
	}

	@Override
	public void onDetach() {
		mOpener = null;
		adapter = null;
		super.onDetach();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		titleET = (EditText) getView().findViewById(R.id.titleText);
		descriptionET = (EditText) getView().findViewById(R.id.descriptionText);
		// startDayET = (EditText) getView().findViewById(R.id.startDayTime);
		// endDayET = (EditText) getView().findViewById(R.id.endDayTime);
		locationET = (EditText) getView().findViewById(R.id.location);
		inviteeAdd = (TextView) getView().findViewById(R.id.userbox);
		startTime = (Button) getView().findViewById(R.id.button1);
		endTime = (Button) getView().findViewById(R.id.button2);
		addPeople = (RelativeLayout) getView().findViewById(
				R.id.relativeLayout1);
		pickLocationBtn = (ImageButton) getView()
				.findViewById(R.id.locationBtn);
		buttonClear = (Button) getView().findViewById(
				R.id.event_create_button_reset);
		buttonUpdate = (Button) getView().findViewById(
				R.id.event_edit_button_update);
		buttonCancel = (Button) getView()
				.findViewById(R.id.event_cancel_button);
		buttonClear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				titleET.setText("" + "");
				descriptionET.setText("" + "");
				startTime.setText("" + "");
				endTime.setText("" + "");
				locationET.setText("" + "");
			}
		});

		addPeople.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mOpener.inviteEventFragment(selectedUser.toArray());
			}
		});
		buttonCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (getResources().getBoolean(R.bool.isTablet) == true) {
					// put
					mOpener.openViewEventFragment(getUniqueKey());
				} else {
					getActivity().finish(); // same as hitting 'back' button
				}
			}
		});
		buttonUpdate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				long numb = 0;
				Event eventData = makeEventDataFromUI();
				if (eventData != null) {
					if (connectionDetector.isConnectingToInternet()) {
						updateEventDataToRemote(eventData);
						eventData.setOffAction(OffAction.N.toString());
						numb = updateEventDataToLocal(eventData);
					} else {
						eventData.setOffAction(OffAction.U.toString());
						numb = updateEventDataToLocal(eventData);
					}
				} else {
					return;
				}
				if (getResources().getBoolean(R.bool.isTablet) == true) {
					mOpener.openViewEventFragment(getUniqueKey());
				} else {
					getActivity().finish(); // same as hitting 'back' button
				}
				Toast.makeText(getActivity(), "Updated" + numb + " Event.",
						Toast.LENGTH_SHORT).show();
			}
		});

		try {
			setValuesToDefault();
		} catch (SQLiteException e) {
			Toast.makeText(getActivity(),
					"Error retrieving information from local data store.",
					Toast.LENGTH_LONG).show();
			Log.e(LOG_TAG, "Error getting Story data from C.P.");
			// e.printStackTrace();
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.event_edit_fragment, container,
				false);
		container.setBackgroundColor(Color.GRAY);
		return view;
	}

	public void doResetButtonClick() {
		setValuesToDefault();
	}

	public void doSaveButtonClick() {
		Event eventData = makeEventDataFromUI();
		if (eventData != null) {
			adapter.updateEvent(eventData, MetaSchema.Event.Cols.ID + " = ? ",
					new String[] { getUniqueKey().toString() });
		} else {
			return;
		}
		if (getResources().getBoolean(R.bool.isTablet) == true) {
			mOpener.openViewEventFragment(getUniqueKey());
		} else {
			getActivity().finish(); // same as hitting 'back' button
		}
		Toast.makeText(getActivity(), "Updated.", Toast.LENGTH_SHORT).show();
	}

	public Event makeEventDataFromUI() {
		// local Editables
		Editable titleCreateable = titleET.getText();
		Editable descriptionCreateable = descriptionET.getText();
		String eventEndTimeCreateable = endTime.getText().toString();
		String eventStartTimeCreateable = startTime.getText().toString();
		String locationCreatable = locationET.getText().toString();

		// Try to parse the date into long format
		try {
			startDate = Event.dateTimeFormat.parse(eventStartTimeCreateable
					.toString());
			endDate = Event.dateTimeFormat.parse(eventEndTimeCreateable
					.toString());
		} catch (ParseException e1) {
			Log.e("CreateStoryFragment",
					"Date was not parsable, reverting to current time");
			startDate = new Date();
			endDate = new Date();
		}

		long eventId = getUniqueKey();
		int colorId = 0;
		String title = "";
		String description = "";
		String location = "";
		long startTime = 0;
		long endTime = 0;
		long createTime = 0;
		long updateTime = 0;
		long userId = 0;
		double latitude = 0;
		double longitude = 0;

		// pull values from Editables
		title = String.valueOf(titleCreateable.toString());
		description = String.valueOf(descriptionCreateable.toString());
		location = String.valueOf(locationCreatable.toString());
		if (place != null) {
			longitude = place.location.lng;
			latitude = place.location.lat;
		}
		startTime = startDate.getTime();
		endTime = endDate.getTime();
		colorId = Event.COLOR_YELLOW;
		updateTime = new Date().getTime();
		Log.i(LOG_TAG, String.valueOf(createTime));
		Log.i(LOG_TAG, String.valueOf(startTime));
		Log.i(LOG_TAG, String.valueOf(endTime));

		// new StoryData object with above info
		Event newData = new Event(colorId, eventId, title, description,
				location, startTime, endTime, longitude, latitude, createTime,
				updateTime, userId);
		newData.setUsersList(selectedUser);
		return newData;

	}

	public void doCancelButtonClick() {
		if (getResources().getBoolean(R.bool.isTablet) == true) {
			// put
			mOpener.openViewEventFragment(getUniqueKey());
		} else {
			getActivity().finish(); // same as hitting 'back' button
		}

	}

	public boolean setValuesToDefault() {

		Event eventData;
		String firstSelected = "Add People";
		selectedUser = new ArrayList<User>();
		try {
			eventData = adapter.getEventsWithAllInvities(getUniqueKey());
			selectedUser.addAll(eventData.getUsersList());
		} catch (SQLException e) {
			Log.d(LOG_TAG, "" + e.getMessage());
			e.printStackTrace();
			return false;
		}

		if (eventData != null) {
			Log.d(LOG_TAG, "setValuesToDefualt :" + eventData.toString());
			// set the EditTexts to the current values
			titleET.setText(eventData.getTitle());
			descriptionET.setText(eventData.getDescription());
			startTime.setText(eventData.getStartDate());
			endTime.setText(eventData.getEndDate());
			createDate = eventData.getCreateDate();
			locationET.setText(eventData.getLocation());
			if (selectedUser != null) {
				if (selectedUser.isEmpty()) {
					inviteeAdd.setText(firstSelected);
				} else {
					int i = 0;
					for (User usr : selectedUser) {
						if (i == 0) {
							firstSelected = usr.getFirstName();
							inviteeAdd.setText(firstSelected);
						} else if (i > 0) {
							inviteeAdd
									.setText(firstSelected + " & " + i + " +");
						}
						i++;
					}

				}
			}
			return true;
		}
		return false;
	}

	public Long getUniqueKey() {
		return getArguments().getLong(rowIdentifyerTAG, 0);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		String firstSelected = "Add People";
		selectedUser = new ArrayList<User>();
		Object[] selecUser;
		Log.i(LOG_TAG, "Entered onActivityResult()");
		locationET = (EditText) getView().findViewById(R.id.location);
		selectedUser = new ArrayList<User>();
		getActivity();
		// TODO - Process the result only if this method received both a
		// RESULT_OK result code and a recognized request code
		// If so, update the Textview showing the user-entered text.
		if ((requestCode == GET_TEXT_REQUEST_CODE)
				&& (resultCode == Activity.RESULT_OK)) {
			String locAddress = data.getExtras().getString("locAddress");
			locationET.setText(locAddress);
			place = (Place) data.getSerializableExtra("place");
		}
		if (requestCode == 2 && (resultCode == Activity.RESULT_OK)) {
			selecUser = (Object[]) data.getSerializableExtra("selectedUser");
			if (selecUser.length == 0) {
				inviteeAdd.setText(firstSelected);
			} else {
				for (int i = 0; i < selecUser.length; i++) {
					User usr = (User) selecUser[i];
					selectedUser.add(usr);
					if (i == 0) {
						firstSelected = usr.getFirstName();
						inviteeAdd.setText(firstSelected);
					} else if (i > 0) {
						inviteeAdd.setText(firstSelected + " & " + i + " +");
					}
				}

			}
		}

	}

	public void updateEventDataToRemote(Event event) {
		final ProgressDialog pDialog = new ProgressDialog(getActivity());
		pDialog.setMessage("Updating Event " + event.getTitle() + "  .....");
		pDialog.show();
		JSONObject eventData = JSONParser.EventObjToJSONObj(event);
		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.PUT,
				GcmConfig.CREATE_APPOINTMENT_URL, eventData,
				new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						Log.d(TAG, response.toString());
						pDialog.hide();
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(TAG, "Error: " + error.getMessage());
						pDialog.hide();
					}
				});

		// Adding request to request queue
		queue.add(jsonObjReq);

	}

	public long updateEventDataToLocal(Event event) {
		// insert it through DBAdapter to be put into sqllitedb
		return adapter.updateEvent(event, MetaSchema.Event.Cols.ID + " = ? ",
				new String[] { getUniqueKey().toString() });
	}

}
