package com.arcmantle.mycal.android.event;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.arcmantle.mycal.android.R;



public class EventActivityBase extends FragmentActivity implements
		OnOpenWindowInterface {

	boolean promptOnBackPressed = false;
	static private int GET_TEXT_REQUEST_CODE=2;
	//EventListFragment fragment;
	private static final String LOG_TAG = EventActivityBase.class
			.getCanonicalName();
	boolean mDualPane;

	@Override
	/**
	 * Handle when the back button is pressed. Overridden to require
	 * confirmation of wanting to exit via back button. 
	 * This functionality can easily be removed.
	 */
	public void onBackPressed() {
		if (promptOnBackPressed == true) {
			new AlertDialog.Builder(this)
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setTitle("Closing Activity")
					.setMessage("Are you sure you want to close this activity?")
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									finish();
								}

							}).setNegativeButton("No", null).show();
		} else {
			super.onBackPressed();
		}
	}

	/**
	 * Determine if the device this app is running on is a tablet or a phone.
	 * 
	 * <p>
	 * phones generally have less than 600dp (this is the threshold we use, this
	 * can be adjusted)
	 * 
	 * @return boolean truth
	 */
	private boolean determineDualPane() {
		if (getResources().getBoolean(R.bool.isTablet) == true) {
			mDualPane = true;
			return true;
		} else {
			mDualPane = false;
			return false;
		}
	}

	/**
	 * Logic required to open the appropriate View EventData Fragment/Activity
	 * combination to display properly on the phone or tablet.
	 */
	public void openViewEventFragment(long index) {
		Log.d(LOG_TAG, "openEventViewFragment(" + index + ")");
		if (determineDualPane()) {

			Fragment test = getSupportFragmentManager().findFragmentById(
					R.id.details);

			// Log.d(LOG_TAG, "open view class:" + test.getClass());
			FragmentTransaction ft = getSupportFragmentManager()
					.beginTransaction();
			if (test != null && test.getClass() != EventViewFragment.class) {
				EventViewFragment details = EventViewFragment
						.newInstance(index);

				// Execute a transaction, replacing any existing
				// fragment with this one inside the frame.
				ft.replace(R.id.details, details);

			} else {
				// Check what fragment is shown, replace if needed.
				EventViewFragment details = (EventViewFragment) getSupportFragmentManager()
						.findFragmentById(R.id.details);
				if (details == null || details.getUniqueKey() != index) {
					// Make new fragment to show this selection.
					details = EventViewFragment.newInstance(index);

				}
				// Execute a transaction, replacing any existing
				// fragment with this one inside the frame.

				ft.replace(R.id.details, details);

			}
			ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			ft.commit();

		} else {
			// Otherwise we need to launch a new activity to display
			// the dialog fragment with selected text.
			Intent intent = newEventViewIntent(this, index);
			startActivity(intent);
		}
	}

	/**
	 * Logic required to open the appropriate Edit EventData Fragment/Activity
	 * combination to display properly on the phone or tablet.
	 */
	public void openEditEventFragment(final long index) {
		Log.d(LOG_TAG, "openEditEventFragment(" + index + ")");
		if (determineDualPane()) {

			Fragment test = getSupportFragmentManager().findFragmentById(
					R.id.details);

			// Log.d(LOG_TAG, "open view class:" + test.getClass());
			FragmentTransaction ft = getSupportFragmentManager()
					.beginTransaction();
			if (test != null && test.getClass() != EditEventFragment.class) {
				EditEventFragment editor = EditEventFragment.newInstance(index);

				// Execute a transaction, replacing any existing
				// fragment with this one inside the frame.

				ft.replace(R.id.details, editor);

			} else {
				// Check what fragment is shown, replace if needed.
				EditEventFragment editor = (EditEventFragment) getSupportFragmentManager()
						.findFragmentById(R.id.details);
				if (editor == null || editor.getUniqueKey() != index) {
					// Make new fragment to show this selection.
					editor = EditEventFragment.newInstance(index);

				}
				// Execute a transaction, replacing any existing
				// fragment with this one inside the frame.

				ft.replace(R.id.details, editor);

			}
			ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			ft.commit();

		} else {
			// Otherwise we need to launch a new activity to display
			// the dialog fragment with selected text.
			Intent intent = newEditEventIntent(this, index);
			startActivity(intent);
		}
	}

	/**
	 * Logic required to open the appropriate Create EventData Fragment/Activity
	 * combination to display properly on the phone or tablet.
	 */
	public void openCreateEventFragment() {
		Log.d(LOG_TAG, "openCreateEventFragment");
		if (determineDualPane()) {

			Fragment test = getSupportFragmentManager().findFragmentById(
					R.id.details);

			// Log.d(LOG_TAG, "open view class:" + test.getClass());
			FragmentTransaction ft = getSupportFragmentManager()
					.beginTransaction();
			if (test != null && test.getClass() != CreateEventFragment.class) {
				CreateEventFragment details = CreateEventFragment.newInstance();

				// Execute a transaction, replacing any existing
				// fragment with this one inside the frame.

				ft.replace(R.id.details, details);

			} else {
				// Check what fragment is shown, replace if needed.
				CreateEventFragment details = (CreateEventFragment) getSupportFragmentManager()
						.findFragmentById(R.id.details);
				if (details == null) {
					// Make new fragment to show this selection.
					details = CreateEventFragment.newInstance();

				}
				// Execute a transaction, replacing any existing
				// fragment with this one inside the frame.

				ft.replace(R.id.details, details);

			}
			ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			ft.commit();

		} else {
			// Otherwise we need to launch a new activity to display
			// the dialog fragment with selected text.
			Intent intent = newCreateEventIntent(this);
			startActivity(intent);
		}
	}

	@Override
	public void openListEventFragment() {
		Log.d(LOG_TAG, "openCreateEventFragment");
		if (determineDualPane()) {
			// already displayed
			Fragment test = getSupportFragmentManager().findFragmentByTag(
					"imageFragmentTag");
			if (test != null) {
				EventListFragment t = (EventListFragment) test;
				t.updateEventData();
			}

		} else {
			// Otherwise we need to launch a new activity to display
			// the dialog fragment with selected text.
			Intent intent = newListEventIntent(this);
			startActivity(intent);
		}
	}
	
	
	@Override
	public void inviteEventFragment(Object[] selectedUser) {
		Log.d(LOG_TAG, "onInviteEventFragment");
		if (determineDualPane()) {
			// already displayed
			Fragment test = getSupportFragmentManager().findFragmentByTag(
					"imageFragmentTag");
			if (test != null) {
				InviteEventFragment t = (InviteEventFragment) test;
				t.updateUserData();
			}

		} else {
			// Otherwise we need to launch a new activity to display
			// the dialog fragment with selected text.
			Intent intent = newInviteEventIntent(this,selectedUser);			
			startActivityForResult(intent, GET_TEXT_REQUEST_CODE);
			//startActivity(intent);
		}
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (determineDualPane()) {
			getSupportFragmentManager().findFragmentById(R.id.details)
					.onActivityResult(requestCode, resultCode, data);
		} else {
			getSupportFragmentManager().findFragmentById(android.R.id.content)
					.onActivityResult(requestCode, resultCode, data);
		}

	}

	/*************************************************************************/
	/*
	 * Create Intents for Intents
	 */
	/*************************************************************************/

	public static Intent newEventViewIntent(Activity activity, long index) {
		Intent intent = new Intent();
		intent.setClass(activity, EventViewActivity.class);
		intent.putExtra(EventViewFragment.rowIdentifyerTAG, index);
		return intent;
	}

	public static Intent newEditEventIntent(Activity activity, long index) {
		Intent intent = new Intent();
		intent.setClass(activity, EditEventActivity.class);
		intent.putExtra(EditEventFragment.rowIdentifyerTAG, index);
		return intent;
	}

	public static Intent newListEventIntent(Activity activity) {
		Intent intent = new Intent();
		intent.setClass(activity, EventListActivity.class);
		return intent;
	}

	public static Intent newCreateEventIntent(Activity activity) {
		Intent intent = new Intent();
		intent.setClass(activity, CreateEventActivity.class);
		return intent;
	}
	
	public static Intent newInviteEventIntent(Activity activity, long index) {
		Intent intent = new Intent();
		intent.setClass(activity, InviteEventActivity.class);
		intent.putExtra(InviteEventFragment.rowIdentifyerTAG, index);
		return intent;
	}
	public static Intent newInviteEventIntent(Activity activity,Object[] selectedUser) {
		Intent intent = new Intent();
		intent.setClass(activity, InviteEventActivity.class);
		intent.putExtra("selectedUser", selectedUser);		
		return intent;
	}

}
