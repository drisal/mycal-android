package com.arcmantle.mycal.android.event;

import java.util.Calendar;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.arcmantle.mycal.android.R;
import com.arcmantle.mycal.android.customview.DateTimePicker;
import com.arcmantle.mycal.android.googlemaps.MapActivity;

/**
 * Fragments require a Container Activity, this is the one for the Edit
 * StoryData, also handles launching intents for audio/video capture.
 */
public class CreateEventActivity extends EventActivityBase implements DateWatcher {
	private final static String LOG_TAG = CreateEventActivity.class
			.getCanonicalName();
	private CreateEventFragment fragment;	
	static private final int GET_TEXT_REQUEST_CODE = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState == null) {
			fragment = CreateEventFragment.newInstance();
			fragment.setArguments(getIntent().getExtras());
			getSupportFragmentManager().beginTransaction()
					.add(android.R.id.content, fragment).commit();		
			
		}
	}
	
	public void onClickStartDayButton(View view){
		 button_click(view,(Button)findViewById(R.id.button1));			
	}
	
	public void onClickEndDayButton(View view){
		 button_click(view,(Button)findViewById(R.id.button2));
	}

	public void button_click(View view, final Button btn) {
		// Create the dialog
		final Dialog mDateTimeDialog = new Dialog(this);
		// Inflate the root layout
		final RelativeLayout mDateTimeDialogView = (RelativeLayout) getLayoutInflater()
				.inflate(R.layout.date_time_dialog, null);
		// Grab widget instance
		final DateTimePicker mDateTimePicker = (DateTimePicker) mDateTimeDialogView
				.findViewById(R.id.DateTimePicker);
		mDateTimePicker.setDateChangedListener(this);

		// Update demo TextViews when the "OK" button is clicked
		((Button) mDateTimeDialogView.findViewById(R.id.SetDateTime))
				.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						mDateTimePicker.clearFocus();
						// TODO Auto-generated method stub
						String result_string =String.valueOf(mDateTimePicker.getDay()) 
								+ "-"
								+ String.valueOf(mDateTimePicker.getMonth())
								+ "-"
								+ mDateTimePicker.getYear()
								+ " "
								+ String.valueOf(mDateTimePicker.getHour())
								+ ":"
								+ String.valueOf(mDateTimePicker.getMinute())
								+ ":00";
								
						// if(mDateTimePicker.getHour() > 12) result_string =
						// result_string + "PM";
						// else result_string = result_string + "AM";
						btn.setText(result_string);
						mDateTimeDialog.dismiss();
					}
				});

		// Cancel the dialog when the "Cancel" button is clicked
		((Button) mDateTimeDialogView.findViewById(R.id.CancelDialog))
				.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						// TODO Auto-generated method stub
						mDateTimeDialog.cancel();
					}
				});

		// Reset Date and Time pickers when the "Reset" button is clicked

		((Button) mDateTimeDialogView.findViewById(R.id.ResetDateTime))
				.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						// TODO Auto-generated method stub
						mDateTimePicker.reset();
					}
				});

		// Setup TimePicker
		// No title on the dialog window
		mDateTimeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// Set the dialog content view
		mDateTimeDialog.setContentView(mDateTimeDialogView);
		// Display the dialog
		mDateTimeDialog.show();
	}

	public void onDateChanged(Calendar c) {
		Log.i("Date Changed:",
				"" + c.get(Calendar.MONTH) + " " + c.get(Calendar.DAY_OF_MONTH)
						+ " " + c.get(Calendar.YEAR) + " "
						+ c.get(Calendar.HOUR_OF_DAY) + " "
						+ c.get(Calendar.MINUTE));
	}
	
	public void gotoMap(View view){	
		
		Intent i = new Intent(this, MapActivity.class);	
		EditText locationET = (EditText)findViewById(R.id.location);
		String location = locationET.getText().toString();
		i.putExtra("location", location);
		// TODO - Start an Activity using that intent and the request code
		// defined above
		startActivityForResult(i, GET_TEXT_REQUEST_CODE);	
	}
	
	/*@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.i(LOG_TAG, "Entered onActivityResult()");
		EditText locationET = (EditText)findViewById(R.id.location);
		// TODO - Process the result only if this method received both a
		// RESULT_OK result code and a recognized request code
		// If so, update the Textview showing the user-entered text.
		if ((requestCode == GET_TEXT_REQUEST_CODE) && (resultCode == RESULT_OK)) {
			String locAddress = data.getExtras().getString("locAddress");
			locationET.setText(locAddress);
		}

	}*/

}
