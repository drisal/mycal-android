/*
The iRemember source code (henceforth referred to as "iRemember") is
copyrighted by Mike Walker, Adam Porter, Doug Schmidt, and Jules White
at Vanderbilt University and the University of Maryland, Copyright (c)
2014, all rights reserved.  Since iRemember is open-source, freely
available software, you are free to use, modify, copy, and
distribute--perpetually and irrevocably--the source code and object code
produced from the source, as well as copy and distribute modified
versions of this software. You must, however, include this copyright
statement along with any code built using iRemember that you release. No
copyright statement needs to be provided if you just ship binary
executables of your software products.

You can use iRemember software in commercial and/or binary software
releases and are under no obligation to redistribute any of your source
code that is built using the software. Note, however, that you may not
misappropriate the iRemember code, such as copyrighting it yourself or
claiming authorship of the iRemember software code, in a way that will
prevent the software from being distributed freely using an open-source
development model. You needn't inform anyone that you're using iRemember
software in your software, though we encourage you to let us know so we
can promote your project in our success stories.

iRemember is provided as is with no warranties of any kind, including
the warranties of design, merchantability, and fitness for a particular
purpose, noninfringement, or arising from a course of dealing, usage or
trade practice.  Vanderbilt University and University of Maryland, their
employees, and students shall have no liability with respect to the
infringement of copyrights, trade secrets or any patents by DOC software
or any part thereof.  Moreover, in no event will Vanderbilt University,
University of Maryland, their employees, or students be liable for any
lost revenue or profits or other special, indirect and consequential
damages.

iRemember is provided with no support and without any obligation on the
part of Vanderbilt University and University of Maryland, their
employees, or students to assist in its use, correction, modification,
or enhancement.

The names Vanderbilt University and University of Maryland may not be
used to endorse or promote products or services derived from this source
without express written permission from Vanderbilt University or
University of Maryland. This license grants no permission to call
products or services derived from the iRemember source, nor does it
grant permission for the name Vanderbilt University or
University of Maryland to appear in their names.
 */

package com.arcmantle.mycal.android.event;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.JsonObjectRequest;
import com.arcmantle.mycal.android.R;
import com.arcmantle.mycal.android.gcm.GcmConfig;
import com.arcmantle.mycal.android.orm.Event;
import com.arcmantle.mycal.android.provider.DBAdapter;
import com.arcmantle.mycal.android.provider.MetaSchema;
import com.arcmantle.mycal.android.util.ConnectionDetector;
import com.arcmantle.mycal.android.util.JSONParser;
import com.arcmantle.mycal.android.util.OffAction;

public class EventViewFragment extends Fragment {

	private static final String LOG_TAG = EventViewFragment.class
			.getCanonicalName();

	private DBAdapter adapter;
	public static String rowIdentifyerTAG = "index";
	static final String TAG = "DELETE EVENT";
	private OnOpenWindowInterface mOpener;

	Event eventData;

	TextView titleTV;
	TextView descriptionTV;
	TextView imageNameTV;
	ImageView locationImage;
	TextView startDayTV;
	TextView endDayTV;
	TextView locationTV;

	// buttons for edit and delete
	Button editButton;
	Button deleteButton;
	
	ConnectionDetector connectionDetector;
	RequestQueue queue;
	OnClickListener myOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			int id = view.getId();
			if (id == R.id.event_delete_button) {
				deleteButtonPressed();
			} else if (id == R.id.event_view_button_edit) {
				editButtonPressed();
			} else if (id == R.id.event_invite_button) {
				inviteButtonPressed();
			} else {
			}

		}
	};

	public static EventViewFragment newInstance(long index) {
		EventViewFragment f = new EventViewFragment();

		// Supply index input as an argument.
		Bundle args = new Bundle();
		args.putLong(rowIdentifyerTAG, index);
		f.setArguments(args);

		return f;
	}

	// this fragment was attached to an activity

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mOpener = (OnOpenWindowInterface) activity;
			adapter = new DBAdapter(activity);
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnOpenWindowListener");
		}
	}

	// this fragment is being created.

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);

	}

	// this fragment is creating its view before it can be modified
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.event_view_fragment, container,
				false);
		container.setBackgroundColor(Color.GRAY);
		return view;
	}

	// this fragment is modifying its view before display
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		titleTV = (TextView) getView().findViewById(R.id.titleText);
		descriptionTV = (TextView) getView().findViewById(R.id.descriptionText);
		startDayTV = (TextView) getView().findViewById(R.id.startDayTime);
		endDayTV = (TextView) getView().findViewById(R.id.endDayTime);
		locationTV = (TextView) getView().findViewById(R.id.location);
		editButton=(Button) getView().findViewById(R.id.event_view_button_edit);
		deleteButton=(Button)getView().findViewById(R.id.event_delete_button);
		//locationImage = (ImageView) getView().findViewById(R.id.locationBtn);
		editButton.setOnClickListener(myOnClickListener);
		deleteButton.setOnClickListener(myOnClickListener);
		try {
			setUiToeventData(getUniqueKey());
		} catch (SQLiteException e) {
			Toast.makeText(getActivity(),
					"Error retrieving information from local data store.",
					Toast.LENGTH_LONG).show();
			Log.e(LOG_TAG, "Error getting Story data from C.P.");
			// e.printStackTrace();
		}
	}

	public void setUiToeventData(Long getUniqueKey) throws SQLiteException {
		Log.d(LOG_TAG, "setUiToeventData");
		
		eventData =(adapter.queryEventData(null,
				MetaSchema.Event.Cols.ID + " = ? ",
				new String[] { getUniqueKey.toString() }, null)).get(0);
		if (eventData == null) {
			getView().setVisibility(View.GONE);
		} else { // else it just displays empty screen
			Log.d(LOG_TAG,
					"setUiToEventData + eventData:" + eventData.toString());
			titleTV.setText(String.valueOf(eventData.getTitle()).toString());
			descriptionTV.setText(String.valueOf(eventData.getDescription())
					.toString());
			startDayTV.setText(eventData.getStartDate());
			endDayTV.setText(eventData.getEndDate());
			locationTV.setText(eventData.getLocation());
		}
	}

	// action to be performed when the edit button is pressed
	private void editButtonPressed() {
		mOpener.openEditEventFragment(eventData.getId());
	}
	
	// action to be performed when the edit button is pressed
		private void inviteButtonPressed() {
			//todo
			//mOpener.inviteEventFragment();
		}

	// action to be performed when the delete button is pressed
	private void deleteButtonPressed() {
		String message;

		message = getResources().getString(
				R.string.event_view_deletion_dialog_message);

		new AlertDialog.Builder(getActivity())
				.setIcon(android.R.drawable.ic_dialog_alert)
				.setTitle(R.string.event_view_deletion_dialog_title)
				.setMessage(message)
				.setPositiveButton(R.string.event_view_deletion_dialog_yes,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								try {
									if (connectionDetector.isConnectingToInternet()) {
										deleteEventDataToRemote(eventData);
										adapter.deleteEvent(eventData.getId());
									} else {
										deleteEventDataToLocal();
									}
									
								} catch (SQLiteException e) {
									Log.e(LOG_TAG, "SQLiteException Caught => "
											+ e.getMessage());
									e.printStackTrace();
								}
								//mOpener.openListEventFragment();
								if (getResources().getBoolean(R.bool.isTablet) == true) {
									mOpener.openViewEventFragment(-1);
								} else {
									getActivity().finish();
								}
							}

						})
				.setNegativeButton(R.string.event_view_deletion_dialog_no, null)
				.show();
	}

	public long getUniqueKey() {
		return getArguments().getLong(rowIdentifyerTAG, 0);
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mOpener = null;
		adapter = null;
	}

	@Override
	public void onResume() {
		super.onResume();
		try {
			setUiToeventData(getUniqueKey());
		} catch (SQLiteException e) {
			Toast.makeText(getActivity(),
					"Error retrieving information from local data store.",
					Toast.LENGTH_LONG).show();
			Log.e(LOG_TAG, "Error getting Story data from C.P.");
		}
	}
	
	public void deleteEventDataToRemote(Event event) {
		final ProgressDialog pDialog = new ProgressDialog(getActivity());
		pDialog.setMessage("Deleting Event " + event.getTitle() + "  .....");
		pDialog.show();
		JSONObject eventData = JSONParser.EventObjToJSONObj(event);
		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.DELETE,
				GcmConfig.CREATE_APPOINTMENT_URL, eventData,
				new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						Log.d(TAG, response.toString());
						pDialog.hide();
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(TAG, "Error: " + error.getMessage());
						pDialog.hide();
					}
				});

		// Adding request to request queue
		queue.add(jsonObjReq);

	}

	public long deleteEventDataToLocal() {
		Long id=eventData.getId();
		eventData.setOffAction(OffAction.D.toString());
		// insert it through DBAdapter to be put into sqllitedb
		return adapter.updateEvent(eventData, MetaSchema.Event.Cols.ID + " = ? ",
				new String[] {id.toString() });
	}

}