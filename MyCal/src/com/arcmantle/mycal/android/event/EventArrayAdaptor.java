package com.arcmantle.mycal.android.event;

import java.util.List;

import com.arcmantle.mycal.android.R;
import com.arcmantle.mycal.android.orm.Event;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class EventArrayAdaptor extends ArrayAdapter<Event> {

    private static final String LOG_TAG = EventArrayAdaptor.class
            .getCanonicalName();

    int resource;

    public EventArrayAdaptor(Context _context, int _resource,
            List<Event> _items) {
        super(_context, _resource, _items);
        Log.d(LOG_TAG, "constructor()");
        resource = _resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d(LOG_TAG, "getView()");
        LinearLayout eventView = null;
        try {
            Event item = getItem(position);
            long KEY_ID = item.getId();
            String title = item.getTitle();
            String startTime=item.getStartDate();
            String endTime=item.getEndDate();
            
            
            if (convertView == null) {
            	eventView = new LinearLayout(getContext());
                String inflater = Context.LAYOUT_INFLATER_SERVICE;
                LayoutInflater vi = (LayoutInflater) getContext()
                        .getSystemService(inflater);
                vi.inflate(resource, eventView, true);
            } else {
            	eventView = (LinearLayout) convertView;
            }

           /* TextView KEY_IDTV = (TextView) eventView
            		.findViewById(R.id.event_listview_custom_row_KEY_ID_textView);*/
            
            TextView titleTV = (TextView) eventView
                    .findViewById(R.id.event_listview_custom_row_title_textView);
            TextView startTimeTV = (TextView) eventView
                    .findViewById(R.id.event_listview_custom_row_creation_starttime_textView);
            TextView endTimeTV = (TextView) eventView
                    .findViewById(R.id.event_listview_custom_row_creation_endtime_textView);
            
            /*KEY_IDTV.setText("" + KEY_ID);*/
            titleTV.setText("" + title);
            startTimeTV.setText("" + startTime);
            endTimeTV.setText("" + endTime);
            Log.i("EventDataArrayAdaptor", startTime);
            
        } catch (Exception e) {
            Toast.makeText(getContext(),
                    "exception in ArrayAdpter: " + e.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
        return eventView;
    }

}