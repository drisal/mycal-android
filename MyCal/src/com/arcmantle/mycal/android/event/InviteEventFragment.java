package com.arcmantle.mycal.android.event;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.arcmantle.mycal.android.R;
import com.arcmantle.mycal.android.orm.User;
import com.arcmantle.mycal.android.provider.DBAdapter;
import com.arcmantle.mycal.android.provider.MetaSchema;

/**
 * Fragments require a Container Activity, this is the one for the Edit
 * StoryData
 */
public class InviteEventFragment extends ListFragment {

	static final String LOG_TAG = InviteEventFragment.class.getCanonicalName();

	OnOpenWindowInterface mOpener;
	DBAdapter adapter;
	ArrayList<User> userList;
	ArrayList<User> allUserList;
	private UserArrayAdaptor aa;
	public final static String rowIdentifyerTAG = "index";
	public static List<User> selectedUser; // store select/unselect information
											// about the values in the list
	EditText filterET;
	Button buttonOk;
	Button buttonCancel;

	// ProgressDialog getServerData;

	/**
	 * @see android.support.v4.app.Fragment#onAttach(android.app.Activity)
	 */
	@Override
	public void onAttach(Activity activity) {
		Log.d(LOG_TAG, "onAttach start");
		super.onAttach(activity);
		try {
			mOpener = (OnOpenWindowInterface) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnOpenWindowListener" + e.getMessage());
		}
		Log.d(LOG_TAG, "onAttach end");
	}

	@Override
	/**
	 * @see android.support.v4.app.Fragment#onDetach()
	 */
	public void onDetach() {
		super.onDetach();
		mOpener = null;
	}

	/**
	 * The system calls this when creating the fragment. Within your
	 * implementation, you should initialize essential components of the
	 * fragment that you want to retain when the fragment is paused or stopped,
	 * then resumed.
	 */
	@Override
	/**
	 * @see android.support.v4.app.Fragment#onCreate(android.os.Bundle)
	 */
	public void onCreate(Bundle savedInstanceState) {
		Log.d(LOG_TAG, "onCreate");
		super.onCreate(savedInstanceState);
		selectedUser = new ArrayList<User>();
		adapter = new DBAdapter(getActivity());
		userList = new ArrayList<User>();
		setRetainInstance(true);
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.app.ListFragment#onCreateView(android.view.LayoutInflater
	 * , android.view.ViewGroup, android.os.Bundle)
	 */
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// getActivity().getIntent().getExtras();
		//selectedUser = new ArrayList<User>();
		Object[] selecUser = (Object[]) getActivity().getIntent()
				.getSerializableExtra("selectedUser");
		if (selecUser != null) {
			if (selecUser.length != 0) {
				for (int i = 0; i < selecUser.length; i++) {
					User usr = (User) selecUser[i];
					selectedUser.add(usr);
				}
			}
		}
		View view = inflater.inflate(R.layout.invite_event_fragment, container,
				false);
		// get the ListView that will be displayed
		ListView lv = (ListView) view.findViewById(android.R.id.list);
		allUserList = adapter.queryUsersData(null,
				MetaSchema.Users.Cols.FIRST_NAME + " LIKE ? ",
				new String[] { "%%" }, MetaSchema.Users.Cols.ID + " DESC");
		filterET = (EditText) view.findViewById(R.id.user_listview_filter);
		filterET.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				updateUserData();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});

		// customize the ListView in whatever desired ways.
		lv.setBackgroundColor(Color.WHITE);
		// return the parent view
		return view;
	}

	//
	// This function is called every time the filter EditText is changed
	// This function should update the ListView to match the specified
	// filter text.
	//

	public void updateUserData() {
		Log.d(LOG_TAG, "updateStoryData");
		try {
			userList.clear();
			String filterWord = filterET.getText().toString();
			adapter.getUsersCount();
			// create String that will match with 'like' in query
			// filterWord = "%" + filterWord + "%";
			userList.addAll(filterUserList(filterWord));
			aa.notifyDataSetChanged();
		} catch (Exception e) {
			Log.e(LOG_TAG,
					"Error connecting to Content Provider" + e.getMessage());
			e.printStackTrace();
		}

	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onActivityCreated(android.os.Bundle)
	 */
	public void onActivityCreated(Bundle savedInstanceState) {
		// create the custom array adapter that will make the custom row
		// layouts
		super.onActivityCreated(savedInstanceState);
		Log.d(LOG_TAG, "onActivityCreated");
		aa = new UserArrayAdaptor(getActivity(),
				R.layout.invitee_list_custom_row, userList);

		// update the back end data.
		updateUserData();
		setListAdapter(aa);
		buttonOk = (Button) getView().findViewById(R.id.invite);
		buttonCancel = (Button) getView().findViewById(R.id.cancel);

		buttonOk.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent data = new Intent();
				data.putExtra("selectedUser", selectedUser.toArray());
				getActivity();
				getActivity().setResult(Activity.RESULT_OK, data);
				getActivity().finish();
			}
		});

		buttonCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (getResources().getBoolean(R.bool.isTablet) == true) {
					// put
					mOpener.openViewEventFragment(getUniqueKey());
				} else {
					getActivity().finish(); // same as hitting 'back' button
				}
			}
		});

	}

	public List<User> filterUserList(String filterWord) {
		List<User> filterList = new ArrayList<User>();
		if (filterWord.isEmpty()) {
			filterList.addAll(allUserList);
		} else {
			for (User usr : allUserList) {
				if (usr.getFirstName().toLowerCase()
						.startsWith(filterWord.toLowerCase())) {
					filterList.add(usr);
				}
			}
		}
		return filterList;
	}

	/*
	 * Refresh story list on fragment resume (rather than having to manually
	 * click a refresh button) (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		super.onResume();
		updateUserData();
	}

	public Long getUniqueKey() {
		return getArguments().getLong(rowIdentifyerTAG, 0);
	}
}
