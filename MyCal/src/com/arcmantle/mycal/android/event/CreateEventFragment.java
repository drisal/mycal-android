package com.arcmantle.mycal.android.event;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.arcmantle.mycal.android.R;
import com.arcmantle.mycal.android.gcm.GcmConfig;
import com.arcmantle.mycal.android.googlemaps.Place;
import com.arcmantle.mycal.android.orm.Event;
import com.arcmantle.mycal.android.orm.User;
import com.arcmantle.mycal.android.provider.DBAdapter;
import com.arcmantle.mycal.android.usersessions.UserSessionManager;
import com.arcmantle.mycal.android.util.ConnectionDetector;
import com.arcmantle.mycal.android.util.Form;
import com.arcmantle.mycal.android.util.JSONParser;
import com.arcmantle.mycal.android.util.OffAction;

/**
 * Fragments require a Container Activity, this is the one for the Edit
 * StoryData
 */
public class CreateEventFragment extends Fragment {

	public final static String LOG_TAG = CreateEventFragment.class
			.getCanonicalName();
	static private final int GET_TEXT_REQUEST_CODE = 1;
	static final String TAG = "CREATE NEW EVENT";
	EditText startDayET, endDayET, descriptionET, titleET, locationET;
	Date startDate, endDate;
	UserSessionManager userSessions;
	Button startTime;
	Button endTime;
	Button buttonCreate;
	Button buttonClear;
	Button buttonInvite;
	RelativeLayout addPeople;
	TextView inviteeAdd;
	Place place;
	TextView latitudeValue;
	TextView longitudeValue;
	ImageButton pickLocationBtn;
	Location loc;
	ConnectionDetector connectionDetector;

	List<User> selectedUser;
	Object[] selecUser;

	// Form used for validation
	private Form mForm;
	static SharedPreferences pref;
	// int index;
	OnOpenWindowInterface mOpener;
	DBAdapter adapter;
	RequestQueue queue;
	
	public final static String LOCATION = "event";

	public static CreateEventFragment newInstance() {
		CreateEventFragment f = new CreateEventFragment();
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		userSessions = new UserSessionManager(getActivity().getApplicationContext());
		setRetainInstance(true);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mOpener = (OnOpenWindowInterface) activity;
			adapter = new DBAdapter(activity);
			pref = activity.getSharedPreferences("AuthorizedToken",
					Context.MODE_PRIVATE);
			connectionDetector = new ConnectionDetector(
					activity.getApplicationContext());
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnOpenWindowListener");
		}
	}

	@Override
	public void onDetach() {
		mOpener = null;
		adapter = null;
		super.onDetach();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		titleET = (EditText) getView().findViewById(R.id.titleText);
		descriptionET = (EditText) getView().findViewById(R.id.descriptionText);
		// startDayET = (EditText) getView().findViewById(R.id.startDayTime);
		// endDayET = (EditText) getView().findViewById(R.id.endDayTime);
		locationET = (EditText) getView().findViewById(R.id.location);
		inviteeAdd = (TextView) getView().findViewById(R.id.userbox);
		pickLocationBtn = (ImageButton) getView()
				.findViewById(R.id.locationBtn);
		startTime = (Button) getView().findViewById(R.id.button1);
		endTime = (Button) getView().findViewById(R.id.button2);
		buttonClear = (Button) getView().findViewById(
				R.id.event_create_button_reset);
		buttonCreate = (Button) getView().findViewById(
				R.id.event_create_button_save);
		buttonInvite = (Button) getView()
				.findViewById(R.id.event_invite_button);
		addPeople = (RelativeLayout) getView().findViewById(
				R.id.relativeLayout1);
		buttonClear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				titleET.setText("" + "");
				descriptionET.setText("" + "");
				startTime.setText("" + "");
				endTime.setText("" + "");
				locationET.setText("" + "");
			}
		});

		addPeople.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mOpener.inviteEventFragment(selecUser);
			}
		});

		buttonCreate.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// local Editables
				Editable titleCreateable = titleET.getText();
				Editable descriptionCreateable = descriptionET.getText();
				String eventEndTimeCreateable = endTime.getText().toString();
				String eventStartTimeCreateable = startTime.getText()
						.toString();
				String locationCreatable = locationET.getText().toString();

				// Try to parse the date into long format
				try {
					startDate = Event.dateTimeFormat
							.parse(eventStartTimeCreateable.toString());
					endDate = Event.dateTimeFormat.parse(eventEndTimeCreateable
							.toString());
				} catch (ParseException e1) {
					Log.e("CreateStoryFragment",
							"Date was not parsable, reverting to current time");
					startDate = new Date();
					endDate = new Date();
				}
				// For future expansion: The loginId and storyId need to be
				// generated by the system
				long eventId = 0;
				int colorId = 0;
				String title = "";
				String description = "";
				String location = "";
				long startTime = 0;
				long endTime = 0;
				long createTime = 0;
				long updateTime = -1;
				long userId = 0;
				double latitude = 0;
				double longitude = 0;

				// pull values from Editables
				title = String.valueOf(titleCreateable.toString());
				description = String.valueOf(descriptionCreateable.toString());
				location = String.valueOf(locationCreatable.toString());
				/*
				 * if (loc != null) { latitude = loc.getLatitude(); longitude =
				 * loc.getLongitude(); }
				 */
				startTime = startDate.getTime();
				endTime = endDate.getTime();
				colorId = Event.COLOR_YELLOW;
				createTime = new Date().getTime();

				userId = adapter.getUserId(pref.getString("auth_token", null));

				eventId = adapter.getEventCount() + 1;
				Log.i(LOG_TAG, String.valueOf(createTime));
				Log.i(LOG_TAG, String.valueOf(startTime));
				Log.i(LOG_TAG, String.valueOf(endTime));
				// new StoryData object with above info
				Event newData = new Event(colorId, eventId, title, description,
						location, startTime, endTime, longitude, latitude,
						createTime, updateTime, userId);
				newData.setUsersList(selectedUser);
				long id=-1;
				if (connectionDetector.isConnectingToInternet())
					saveEventDataToRemote(newData);
				else
					id=saveEventDataToLocal(newData);
				Log.d(CreateEventFragment.class.getCanonicalName(),
						"newEventData: " + newData + "eventId: " + eventId);
				
				Log.i(LOG_TAG, "id: " + id);
				// return back to proper state
				if (getResources().getBoolean(R.bool.isTablet) == true) {
					// put
					mOpener.openViewEventFragment(id);
				} else {
					getActivity().finish(); // same as hitting 'back' button
				}
			}
		});

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		String firstSelected = "";
		Log.i(LOG_TAG, "Entered onActivityResult()");
		locationET = (EditText) getView().findViewById(R.id.location);
		selectedUser = new ArrayList<User>();
		getActivity();
		// TODO - Process the result only if this method received both a
		// RESULT_OK result code and a recognized request code
		// If so, update the Textview showing the user-entered text.
		if ((requestCode == GET_TEXT_REQUEST_CODE)
				&& (resultCode == Activity.RESULT_OK)) {
			String locAddress = data.getExtras().getString("locAddress");
			locationET.setText(locAddress);
			place = (Place) data.getSerializableExtra("place");
		}
		if (requestCode == 2 && (resultCode == Activity.RESULT_OK)) {
			selecUser = (Object[]) data.getSerializableExtra("selectedUser");
			if (selecUser.length == 0) {
				inviteeAdd.setText("Add People");
			} else {
				for (int i = 0; i < selecUser.length; i++) {
					User usr = (User) selecUser[i];
					selectedUser.add(usr);
					if (i == 0) {
						firstSelected = usr.getFirstName();
						inviteeAdd.setText(firstSelected);
					} else if (i > 0) {
						inviteeAdd.setText(firstSelected + " & " + i + " +");
					}
				}

			}
		}

	}

	public void setLocation(Location location) {
		Log.d(LOG_TAG, "setLocation =" + location);
		loc = location;
		double latitude = loc.getLatitude();
		double longitude = loc.getLongitude();

		latitudeValue.setText("" + latitude);
		longitudeValue.setText("" + longitude);
	}

	public Event setEventData() {
		// local Editables
		Editable titleCreateable = titleET.getText();
		Editable descriptionCreateable = descriptionET.getText();
		String eventEndTimeCreateable = endTime.getText().toString();
		String eventStartTimeCreateable = startTime.getText().toString();
		String locationCreatable = locationET.getText().toString();

		// Try to parse the date into long format
		try {
			startDate = Event.dateTimeFormat.parse(eventStartTimeCreateable
					.toString());
			endDate = Event.dateTimeFormat.parse(eventEndTimeCreateable
					.toString());
		} catch (ParseException e1) {
			Log.e("CreateStoryFragment",
					"Date was not parsable, reverting to current time");
			startDate = new Date();
			endDate = new Date();
		}
		// For future expansion: The loginId and storyId need to be
		// generated by the system
		long eventId = 0;
		int colorId = 0;
		String title = "";
		String description = "";
		String location = "";
		long startTime = 0;
		long endTime = 0;
		long createTime = 0;
		long updateTime = -1;
		long userId = 0;
		double latitude = 0;
		double longitude = 0;

		// pull values from Editables
		title = String.valueOf(titleCreateable.toString());
		description = String.valueOf(descriptionCreateable.toString());
		location = String.valueOf(locationCreatable.toString());
		/*
		 * if (loc != null) { latitude = loc.getLatitude(); longitude =
		 * loc.getLongitude(); }
		 */
		startTime = startDate.getTime();
		endTime = endDate.getTime();
		colorId = Event.COLOR_YELLOW;
		createTime = new Date().getTime();
		longitude = place.location.lng;
		latitude = place.location.lat;
		userId = adapter.getUserId(pref.getString("auth_token", null));
		eventId = adapter.getEventCount() + 1;
		Log.i(LOG_TAG, String.valueOf(createTime));
		Log.i(LOG_TAG, String.valueOf(startTime));
		Log.i(LOG_TAG, String.valueOf(endTime));

		// new StoryData object with above info
		Event newData = new Event(colorId, eventId, title, description,
				location, startTime, endTime, longitude, latitude, createTime,
				updateTime, userId);
		Log.d(CreateEventFragment.class.getCanonicalName(), "newEventData: "
				+ newData + "eventId: " + eventId);
		// insert it through DBAdapter to be put into sqllitedb

		return newData;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.event_creation_fragment,
				container, false);
		container.setBackgroundColor(Color.GRAY);
		return view;
	}

	public void saveEventDataToRemote(Event event) {
		queue = Volley.newRequestQueue(getActivity());
		/*final ProgressDialog pDialog = new ProgressDialog(getActivity());
		pDialog.setMessage("Creating Event "+event.getTitle()+"  ....");
		pDialog.show();  */   
		JSONObject eventData=JSONParser.EventObjToJSONObj(event) ;
		// Set Request parameter
		String data="";		
		try {
			data+="&auth_token=" +URLEncoder.encode(userSessions.getAuthToken(),"UTF-8");
			data+= "&" + URLEncoder.encode("appointment", "UTF-8") + "="
					+ eventData.toString();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		makeGetPostRequest(GcmConfig.CREATE_APPOINTMENT_URL,data);
		/*JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.POST,
				GcmConfig.CREATE_APPOINTMENT_URL, data, new Response.Listener<JSONObject>() {		 
		                    @Override
		                    public void onResponse(JSONObject response) {
		                        Log.d(TAG, response.toString());
		                        Toast.makeText(getActivity(),
		                        		"Response => "+response.toString(),
		            					Toast.LENGTH_LONG).show();
		                        //pDialog.hide();
		                    }
		                }, new Response.ErrorListener() {		 
		                    @Override
		                    public void onErrorResponse(VolleyError error) {
		                        VolleyLog.d(TAG, "Error: " + error.getMessage());
		                       // pDialog.hide();
		                    }							
		                });
		 
		// Adding request to request queue
		queue.add(jsonObjReq);*/

	}

	public long saveEventDataToLocal(Event event) {
		// insert it through DBAdapter to be put into sqllitedb
		event.setOffAction(OffAction.C.toString());
		return adapter.createEvent(event);
	}
	
	public String makeGetPostRequest(String urls, String data) {
		BufferedReader reader = null;
		StringBuilder sb=null;
		try {
			// Defined URL where to send data
			URL url = new URL(urls);
			// Send POST data request
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter wr = new OutputStreamWriter(
					conn.getOutputStream());
			wr.write(data);
			wr.flush();
			// Get the server response
			reader = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			sb = new StringBuilder();
			String line = null;
			// Read Server Response
			while ((line = reader.readLine()) != null) {
				// Append server response in string
				sb.append(line + "");
			}
			
		} catch (Exception ex) {
			ex.getMessage();
		} finally {
			try {
				reader.close();				
			} catch (Exception ex) {
			}
		}
		return sb.toString();
	}

}
