package com.arcmantle.mycal.android.event;

import java.util.Calendar;

public interface DateWatcher {	
	void onDateChanged(Calendar c);

}
