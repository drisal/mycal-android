package com.arcmantle.mycal.android.googlemaps;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.arcmantle.mycal.android.R;
import com.arcmantle.mycal.android.util.LocationFinder;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends Activity implements OnMapClickListener,
		OnMapLongClickListener, OnMarkerDragListener {

	// Google Map
	private GoogleMap googleMap;
	final int RQS_GooglePlayServices = 1;
	Location myLocation;
	boolean markerClicked;
	ImageView searchBtn;
	LatLng latlong;
	EditText tvLocInfo;
	MarkerOptions marker;
	Button okBtn;	
	// action bar
	private ActionBar actionBar;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		tvLocInfo = (EditText) findViewById(R.id.req_location);
		searchBtn=(ImageView) findViewById(R.id.search_btn);
		okBtn=(Button) findViewById(R.id.select_location);
		actionBar = getActionBar();

		// Hide the action bar title
		actionBar.setDisplayShowTitleEnabled(false);

		try {
			// Loading map
			initilizeMap();	
			// Defining button click event listener for the find button
		    OnClickListener findClickListener = new OnClickListener() {
		        @Override
		        public void onClick(View v) {
		            // Getting user input location
		            String location = tvLocInfo.getText().toString();	 
		            if(location!=null && !location.equals("")){
		                new GeocoderTask().execute(location);
		            }
		        }
		    };
	 
	        // Setting button click event listener for the find button
	        searchBtn.setOnClickListener(findClickListener);
	        okBtn.setOnClickListener(okClickListener);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	OnClickListener okClickListener = new OnClickListener() {
	        @Override
	        public void onClick(View v) {
	            // Getting user input location	        
	        	Intent data = new Intent();
	        	if(latlong==null)
	        	latlong=getLocation();
	    		//String position = marker.getPosition().toString();
	    		String locAddress=tvLocInfo.getText().toString();	    		
	    		data.putExtra("locAddress", locAddress);	    		
	    		Place place=new Place();
	    		place.location.lat=latlong.latitude;
	    		place.location.lng=latlong.longitude;
	    		data.putExtra("place", place);	
	    		//Sdata.putExtra("position", position);
	    		// TODO - Set Activity's result with result code RSULT_OK
	    		setResult(RESULT_OK, data);	    		
	    		//startActivityForResult(data, RESULT_OK)	
	    		finish();;          
	        }
	    };
	/**
	 * function to load map. If map is not created it will create it for you
	 * */
	private void initilizeMap() {
		if (googleMap == null) {
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map)).getMap();
			// check if map is created successfully or not
			if (googleMap == null) {
				Toast.makeText(getApplicationContext(),
						"Sorry! unable to create maps", Toast.LENGTH_SHORT)
						.show();
			} else {
				googleMap.setMyLocationEnabled(true);
				googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

				googleMap.setOnMapClickListener(this);
				// googleMap.setOnMapLongClickListener(this);
				googleMap.setOnMarkerDragListener(this);

				CameraUpdate center = CameraUpdateFactory
						.newLatLng(getLocation());
				CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);

				googleMap.moveCamera(center);
				googleMap.animateCamera(zoom);
				marker = new MarkerOptions();
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		initilizeMap();
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(getApplicationContext());

		if (resultCode == ConnectionResult.SUCCESS) {
			Toast.makeText(getApplicationContext(),
					"isGooglePlayServicesAvailable SUCCESS", Toast.LENGTH_LONG)
					.show();
		} else {
			GooglePlayServicesUtil.getErrorDialog(resultCode, this,
					RQS_GooglePlayServices);
		}

	}

	@Override
	public void onMapClick(LatLng point) {
		setAddress(point);
		if (marker != null) {
			googleMap.clear();
		}
		googleMap.addMarker(marker.position(point).draggable(true));
		markerClicked = false;
	}

	@Override
	public void onMapLongClick(LatLng point) {
		setAddress(point);
		if (marker != null) {
			googleMap.clear();
		}
		googleMap.addMarker(marker.position(point).draggable(true));
		markerClicked = false;
	}

	@Override
	public void onMarkerDrag(Marker marker) {		
	}

	@Override
	public void onMarkerDragEnd(Marker marker) {		
	}

	@Override
	public void onMarkerDragStart(Marker marker) {		

	}
	// An AsyncTask class for accessing the GeoCoding Web Service
    private class GeocoderTask extends AsyncTask<String, Void, List<Address>>{
 
        @Override
        protected List<Address> doInBackground(String... locationName) {
            // Creating an instance of Geocoder class
            Geocoder geocoder = new Geocoder(getBaseContext());
            List<Address> addresses = null;
 
            try {
                // Getting a maximum of 3 Address that matches the input text
                addresses = geocoder.getFromLocationName(locationName[0], 3);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return addresses;
        }
 
        @Override
        protected void onPostExecute(List<Address> addresses) {
 
            if(addresses==null || addresses.size()==0){
                Toast.makeText(getBaseContext(), "No Location found", Toast.LENGTH_SHORT).show();
            }
 
            // Clears all the existing markers on the map
            googleMap.clear();
 
            // Adding Markers on Google Map for each matching address
            for(int i=0;i<addresses.size();i++){
 
                Address address = (Address) addresses.get(i);
 
                // Creating an instance of GeoPoint, to display in Google Map
                LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
 
                String addressText = String.format("%s, %s",
                address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
                address.getCountryName());
 
                marker = new MarkerOptions();
                marker.position(latLng);
                marker.title(addressText);
 
                googleMap.addMarker(marker);
 
                // Locate the first location
                if(i==0)
                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
            }
        }
    }

	public LatLng getLocation() {
		LocationFinder myLoc = new LocationFinder(MapActivity.this);
		double latitude = 0;
		double longitude = 0;
		// check if GPS enabled
		if (myLoc.canGetLocation()) {
			latitude = myLoc.getLatitude();
			longitude = myLoc.getLongitude();
			// \n is for new line
			Toast.makeText(
					getApplicationContext(),
					"Your Location is - \nLat: " + latitude + "\nLong: "
							+ longitude, Toast.LENGTH_LONG).show();
		} else {
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings
			myLoc.showSettingsAlert();
		}

		return new LatLng(latitude, longitude);
	}

	public void setAddress(LatLng latlng) {
		Geocoder geocoder;
		List<Address> addresses;
		geocoder = new Geocoder(this, Locale.getDefault());
		try {
			addresses = geocoder.getFromLocation(latlng.latitude,
					latlng.longitude, 1);
			String address = addresses.get(0).getAddressLine(0);
			String city = addresses.get(0).getAddressLine(1);
			//String country = addresses.get(0).getAddressLine(2);
			tvLocInfo.setText(address+","+city);
			latlong=latlng;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}	
	

}