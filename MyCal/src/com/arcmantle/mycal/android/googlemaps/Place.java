package com.arcmantle.mycal.android.googlemaps;

import java.io.Serializable;

import android.graphics.Bitmap;

import com.google.api.client.util.Key;

public class Place implements Serializable {

	@Key
	public String id;

	@Key
	public String name;

	@Key
	public String reference;

	@Key
	public String icon;

	@Key
	public Bitmap mapImg;

	@Key
	public String vicinity;

	@Key
	public Location location;

	@Key
	public String formatted_address;

	public Place (){
		location=new Location();
	}
	@Override
	public String toString() {
		return name + " - " + id + " - " + reference;
	}

	public static class Location implements Serializable {
		@Key
		public double lat;

		@Key
		public double lng;
	}

}
