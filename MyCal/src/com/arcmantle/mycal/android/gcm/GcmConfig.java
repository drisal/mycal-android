package com.arcmantle.mycal.android.gcm;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

import com.arcmantle.mycal.android.util.Constant;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class GcmConfig {

	
	static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	public static final String GOOGLE_SENDER_ID = "144804592748";
	static final String TAG = "GCM REGISTRATION";
	public static final String PROPERTY_REG_ID = "registration_id";
	public static final String PROPERTY_APP_VERSION = "appVersion";
	public static String PREFER_NAME = "gcm_regid";
	
	//USER POST METHOD URL
	public static final String USER_REGISTRATION_URL=Constant.BASE_URL+"/api/v1/users.json";
	public static final String USER_LOGIN_URL=Constant.BASE_URL+"/api/v1/users/authenticate.json";
	
	//USER GET METHOD URL	
	public static final String GET_USER_INFO_URL=Constant.BASE_URL+"/api/v1/users.json";
	
	
	public static final String USER_INFO_BY_ID_URL=Constant.BASE_URL+"/api/v1/users/";
	
	
	
	//EVENT POST METHOD URL
	public static final String CREATE_APPOINTMENT_URL=Constant.BASE_URL+"/api/v1/appointments.json";
	//EVENT GET METHOD URL
	public static final String GET_APPOINTENET_URL=Constant.BASE_URL+"/api/v1/appointments.json";
	
	
	public static final String APPOINTMENT_BY_ID_URL=Constant.BASE_URL+"/api/v1/appointments/";
	
	/**
	 * Check the device to make sure it has the Google Play Services APK. If it
	 * doesn't, display a dialog that allows users to download the APK from the
	 * Google Play Store or enable it in the device's system settings.
	 */
	public static boolean checkPlayServices(Context context, Activity activity) {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(context);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, activity,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i(TAG, "This device is not supported.");
				activity.finish();
			}
			return false;
		}
		return true;
	}
	
	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	public static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}
	
	
	public static String getUrlByUserId(String id){
		return USER_INFO_BY_ID_URL+id +".json";
	} 
	
	public static String getUrlByAppointmentId(String id){
		return APPOINTMENT_BY_ID_URL+id +".json";
	} 
	
}
