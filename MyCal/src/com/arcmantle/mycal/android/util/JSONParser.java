package com.arcmantle.mycal.android.util;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.arcmantle.mycal.android.orm.Event;
import com.arcmantle.mycal.android.orm.User;
import com.arcmantle.mycal.android.provider.DBAdapter;

public class JSONParser {

	public List<User> SyncJsonArrayToUserList(JSONArray jsonArray,
			Context appContext) {
		List<User> usrList = new ArrayList<User>();
		DBAdapter adapter = new DBAdapter(appContext);
		JSONObject jsonObj;
		if (adapter.deleteAllUsers()) {
			for (int i = 0; i < jsonArray.length(); i++) {
				try {
					jsonObj = jsonArray.getJSONObject(i);
					User usr = new User(Long.valueOf(jsonObj.getString("id")),
							jsonObj.getString("username"),
							jsonObj.getString("email"),
							jsonObj.getString("contact_number"),
							jsonObj.getString("full_name"), "",
							jsonObj.getString("created_at"), "0",
							jsonObj.getString("auth_token"));
					adapter.createUser(usr);
					usrList.add(usr);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return usrList;
	}

	public List<Event> SyncJsonArrayToEventList(JSONArray jsonArray,
			Context appContext) {
		List<Event> eventList = new ArrayList<Event>();
		List<User> userList = new ArrayList<User>();
		DBAdapter adapter = new DBAdapter(appContext);
		JSONObject jsonObj;
		if (adapter.getEventCount() > 0) {
			adapter.deleteAllEvents();
		}
		for (int i = 0; i < jsonArray.length(); i++) {
			try {
				jsonObj = jsonArray.getJSONObject(i);
				String invitees = jsonObj.optString("invitees");
				
				String longitude=jsonObj.optString("longitude");
				String latitude=jsonObj.optString("latitude");
				
				if (!invitees.isEmpty()) {
					String[] invitee=invitees.split(",");
					for (int j = 0; j< invitee.length; j++) {
						User usr = new User(Long.valueOf(invitee[j]));
						userList.add(usr);
					}
				}
				Event event = new Event(1,
						Long.valueOf(jsonObj.getString("id")),
						jsonObj.getString("title"),
						jsonObj.getString("description"),
						jsonObj.getString("location"),
						Event.getDateTime(jsonObj.getString("start_time")), 
						Event.getDateTime(jsonObj.getString("end_time")),
						Double.valueOf(longitude.isEmpty()?"0":longitude),
						Double.valueOf(latitude.isEmpty()?"0":latitude),
						Event.getDateTime(jsonObj.getString("created_at")),
						Event.getDateTime(jsonObj.getString("updated_at")),
						Long.valueOf(jsonObj.getString("user_id")), "N");
				event.setUsersList(userList);
				adapter.createEvent(event);
				eventList.add(event);
			} catch (JSONException e) {
				//
				e.printStackTrace();
			}

		}
		return eventList;
	}

	public static Long SaveNotificationToEvent(JSONObject eventObj,
			Context appContext) {
		List<User> userList = new ArrayList<User>();		
		Long eventId = null;
		DBAdapter adapter = new DBAdapter(appContext);
		try {
			String invitees = eventObj.optString("invitees");
			String longitude=eventObj.optString("longitude");
			String latitude=eventObj.optString("latitude");			
			if (!invitees.isEmpty()) {
				String[] invitee=invitees.split(",");
				for (int j = 0; j< invitee.length; j++) {
					User usr = new User(Long.valueOf(invitee[j]));
					userList.add(usr);
				}
			}
			Event event = new Event(1, Long.valueOf(eventObj.getString("id")),
					eventObj.getString("title"),
					eventObj.getString("description"),
					eventObj.getString("location"),
					Event.getDateTime(eventObj.getString("start_time")),
					Event.getDateTime(eventObj.getString("end_time")),
					Double.valueOf(longitude.isEmpty()?"0":longitude),
					Double.valueOf(latitude.isEmpty()?"0":latitude),
					Event.getDateTime(eventObj.getString("created_at")),
					Event.getDateTime(eventObj.getString("updated_at")),
					Long.valueOf(eventObj.getString("user_id")), "N");
			event.setUsersList(userList);
			eventId = adapter.createEvent(event);

		} catch (JSONException e) {
			//
			e.printStackTrace();
		}

		return eventId;
	}

	public static JSONObject EventObjToJSONObj(Event event) {
		JSONObject eventObj = new JSONObject();
		try {
			String invitees = "";
			eventObj.put("title", event.getTitle());
			eventObj.put("description", event.getDescription());
			eventObj.put("location", event.getLocation());
			eventObj.put("start_time", event.getISODateTime(event.getStart()));
			eventObj.put("end_time", event.getISODateTime(event.getEnd()));
			// eventObj.put("longitude", event.getLongitude());
			// eventObj.put("latitude", event.getLatitude());
			// eventObj.put("create_time",
			// event.getISODateTime(event.getCreateTime()));
			// eventObj.put("update_time",
			// event.getISODateTime(event.getUpdateTime()));
			// eventObj.put("userId", event.getUserId());
			if (event.getUsersList() != null) {
				for (User usr : event.getUsersList()) {
					invitees += usr.getUserId() + ",";
				}
				invitees = invitees.substring(0, invitees.length() - 1);
			}
			eventObj.put("invitees", invitees);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return eventObj;

	}

}
