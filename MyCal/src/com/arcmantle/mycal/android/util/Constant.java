package com.arcmantle.mycal.android.util;

public class Constant {
	public static final String FIRST_COLUMN = "blank";
	public static final String SECOND_COLUMN = "Sunday";	
	public static final String THIRD_COLUMN = "Monday";
	public static final String FOURTH_COLUMN = "Tuesday";
	public static final String FIFTH_COLUMN = "Wednesday";
	public static final String SIXTH_COLUMN = "Thursday";
	public static final String SEVENTH_COLUMN = "Friday";
	public static final String EIGHT_COLUMN = "Saturday";
	
	public static final String BASE_URL="http://104.236.37.226:3000";
	public static final String GOOGLE_SENDER_ID="144804592748";

}
