package com.arcmantle.mycal.android.util.validations;

import android.content.Context;
import android.widget.EditText;

import com.arcmantle.mycal.android.R;
import com.arcmantle.mycal.android.util.Field;
import com.arcmantle.mycal.android.util.ValidationResult;

public class IsPositiveInteger extends BaseValidation {

    public static final String POSITIVE_INT_PATTERN = "\\d+";

    private IsPositiveInteger(Context context) {
        super(context);
    }

    public static Validation build(Context context) {
        return new IsPositiveInteger(context);
    }

    @Override
    public ValidationResult validate(Field field) {
        EditText textView = field.getTextView();
        boolean isValid = textView.getText().toString().matches(POSITIVE_INT_PATTERN);
        return isValid ?
            ValidationResult.buildSuccess(textView)
            : ValidationResult.buildFailed(textView, mContext.getString(R.string.zvalidations_not_positive_integer));
    }
}
