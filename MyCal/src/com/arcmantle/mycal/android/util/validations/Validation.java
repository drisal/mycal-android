package com.arcmantle.mycal.android.util.validations;

import com.arcmantle.mycal.android.util.Field;
import com.arcmantle.mycal.android.util.ValidationResult;

public interface Validation {

    ValidationResult validate(Field field);

}
