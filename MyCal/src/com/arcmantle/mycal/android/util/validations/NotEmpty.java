package com.arcmantle.mycal.android.util.validations;


import android.content.Context;
import android.text.TextUtils;

import com.arcmantle.mycal.android.R;
import com.arcmantle.mycal.android.util.Field;
import com.arcmantle.mycal.android.util.ValidationResult;

public class NotEmpty extends BaseValidation {

    public static Validation build(Context context) {
        return new NotEmpty(context);
    }

    private NotEmpty(Context context) {
        super(context);
    }

    @Override
    public ValidationResult validate(Field field) {
        boolean isValid = !TextUtils.isEmpty(field.getTextView().getText());
        return isValid ?
            ValidationResult.buildSuccess(field.getTextView())
            : ValidationResult.buildFailed(field.getTextView(), mContext.getString(R.string.zvalidations_empty));
    }
}
