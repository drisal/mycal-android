package com.arcmantle.mycal.android.util.validations;

import android.content.Context;
import android.widget.EditText;

import com.arcmantle.mycal.android.R;
import com.arcmantle.mycal.android.util.Field;
import com.arcmantle.mycal.android.util.ValidationResult;


public class InRange extends BaseValidation {

    private int mMin;
    private int mMax;

    private InRange(Context context, int min, int max) {
        super(context);
        mMin = min;
        mMax = max;
    }

    public static Validation build(Context context, int min, int max) {
        return new InRange(context, min, max);
    }

    @Override
    public ValidationResult validate(Field field) {
        EditText textView = field.getTextView();
        boolean isValid = false;
        try {
            int value = Integer.parseInt(textView.getText().toString());
            isValid = (value > mMin) && (value < mMax);
        } catch (NumberFormatException ignored) {
        }
        return isValid ?
            ValidationResult.buildSuccess(textView)
            : ValidationResult.buildFailed(textView, mContext.getString(R.string.zvalidations_not_in_range, mMin, mMax));
    }
}
