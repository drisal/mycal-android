package com.arcmantle.mycal.android.util;

public enum OffAction {
	C("CREATE"), U("UPDATE"), D("DELETE"),N("NONE");

	private String def;

	OffAction(String def) {
		this.def = def;
	}
	
	String getDef(){
		return this.def;
	}
	
	@Override
	public String toString(){
		return name();
	}

}
