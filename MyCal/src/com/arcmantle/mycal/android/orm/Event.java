package com.arcmantle.mycal.android.orm;


import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import android.graphics.Bitmap;

public class Event implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int color;
	private long id;
	private String title;
	private String description;
	private String location;
	public long start;
	public long end;
	private Bitmap image;	
	private double longitude;
	private double latitude;
	public long createTime;
	public long updateTime;
	private long userId;
	private String offAction="N";
	
	public static final int DEFAULT_EVENT_ICON = 0;
	public static final int COLOR_RED = 1;
	public static final int COLOR_BLUE = 2;
	public static final int COLOR_YELLOW = 3;
	public static final int COLOR_PURPLE = 4;
	public static final int COLOR_GREEN = 5;
	static DateTimeZone timeZone = DateTimeZone.forID(Locale.getDefault().getCountry() );
	public static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss",Locale.getDefault());
	static DateTimeFormatter formatter = DateTimeFormat.forPattern( "dd-MMM-yyyy HH:mm:ss" ).withZone(timeZone);
	public static SimpleDateFormat forJodaTimeFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss",Locale.getDefault());
	private List<User> usersList=new ArrayList<User>();
	
	public Event(long eventID, long startMills, long endMills){
		this.id = eventID;
		this.start = startMills;
		this.end = endMills;
	}
	
	public Event(int color, long id, String title, String description,
			String location, long start, long end,double longitude, double latitude, long createTime, long updateTime,long userId) {
		super();
		this.color = color;
		this.id = id;
		this.title = title;
		this.description = description;
		this.location = location;
		this.start = start;
		this.end = end;		
		this.longitude = longitude;
		this.latitude = latitude;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.userId=userId;
	}

	public Event(int color, long id, String title, String description,
			String location, long start, long end,double longitude, double latitude, long createTime, long updateTime,long userId,String offAction) {
		super();
		this.color = color;
		this.id = id;
		this.title = title;
		this.description = description;
		this.location = location;
		this.start = start;
		this.end = end;		
		this.longitude = longitude;
		this.latitude = latitude;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.userId=userId;
		this.offAction=offAction;
	}


	public int getColor(){
		return color;
	}
	
	public void setColor(int color){
		this.color = color;
	}
	
	/**
	 * Get the event title
	 * 
	 * @return title
	 */
	public String getTitle(){
		return title;
	}
	
	/**
	 * Get the event description
	 * 
	 * @return description
	 */
	public String getDescription(){
		return description;
	}
	
	
	public Bitmap getImage(){
		return image;
	}
	
	public void setDescription(String description){
		this.description = description;
	}
	
	public void setLocation(String location){
		this.location = location;
	}
	
	public String getLocation(){
		return location;
	}
	
	/**
	 * Set the title of the event
	 * 
	 * @param title
	 */
	public void setTitle(String title){
		this.title = title;
	}
	
	/**
	 * Gets the event id in the database
	 * 
	 * @return event database id
	 */
	public long getId(){
		return id;
	}
	
	/**
	 * Get the start date of the event
	 * 
	 * @return start date
	 */
	public String getStartDate(){		
		String date = dateTimeFormat.format(start);
		
		return date;
	}
	
	/**
	 * Get the end date of the event
	 * 
	 * @return end date
	 */
	public String getEndDate(){		
		String date = dateTimeFormat.format(end);
		
		return date;
	}

	/**
	 * Get the end date of the event
	 * 
	 * @return end date
	 */
	public String getCreateDate(){		
		String date = dateTimeFormat.format(createTime);		
		return date;
	}
	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public void setId(long eventId) {
		this.id = eventId;
	}

	public List<User> getUsersList() {
		return usersList;
	}
	public void setUsersList(List<User> usersList) {
		this.usersList = usersList;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(long updateTime) {
		this.updateTime = updateTime;
	}

	public String getOffAction() {
		return offAction;
	}

	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public long getEnd() {
		return end;
	}

	public void setEnd(long end) {
		this.end = end;
	}

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	public void setOffAction(String offAction) {
		this.offAction = offAction;
	}	
	
	public static String getISODateTime(long time){
		String date = forJodaTimeFormat.format(time);
		DateTime dateTime = formatter.parseDateTime(date);
		return dateTime.toString();
	}
	
	public static Long getDateTime(String iSODate){
		DateTime dateTime2 = new DateTime( iSODate, timeZone );
		return dateTime2.getMillis();
	}
}
