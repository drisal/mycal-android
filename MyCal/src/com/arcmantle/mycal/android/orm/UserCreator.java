package com.arcmantle.mycal.android.orm;

import java.util.ArrayList;

import android.content.ContentValues;
import android.database.Cursor;

import com.arcmantle.mycal.android.provider.MetaSchema;


/**
 * UsersCreator is a helper class that does convenience functions for converting
 * between the Custom ORM objects, ContentValues, and Cursors.
 * 
 * @author Deep Risal
 * 
 */
public class UserCreator {
	
	/**
	 * Create a ContentValues from a provided Users.
	 * 
	 * @param data
	 *            Users to be converted.
	 * @return ContentValues that is created from the Users object
	 * 
	 * 
	 **/
	

	public static ContentValues getCVfromInvitee(final User data) {
		ContentValues rValue = new ContentValues();
		rValue.put(MetaSchema.Users.Cols.ID, data.getUserId());
		rValue.put(MetaSchema.Users.Cols.UID, data.getuId());
		rValue.put(MetaSchema.Users.Cols.EMAIL_ID, data.getEmailId());
		rValue.put(MetaSchema.Users.Cols.PHONE_NO, data.getPhoneNo());
		rValue.put(MetaSchema.Users.Cols.FIRST_NAME, data.getFirstName());
		rValue.put(MetaSchema.Users.Cols.LAST_NAME, data.getLastName());
		rValue.put(MetaSchema.Users.Cols.CREATED_AT, data.getCreatedAt());
		rValue.put(MetaSchema.Users.Cols.AUTH_KEY, data.getAuthKey());
		rValue.put(MetaSchema.Users.Cols.IS_INVITEE, data.isInvitee()?"1":"0");
		return rValue;
	}

	/**
	 * Get all of the Users from the passed in cursor.
	 * 
	 * @param cursor
	 *            passed in cursor to get Users(s) of.
	 * @return ArrayList<Users\> The set of Users
	 */
	public static ArrayList<User> getInviteeDataArrayListFromCursor(Cursor cursor) {
		ArrayList<User> rValue = new ArrayList<User>();
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {
					rValue.add(getUsersDataFromCursor(cursor));
				} while (cursor.moveToNext() == true);
			}
		}
		return rValue;
	}

	/**
	 * Get the first Users from the passed in cursor.
	 * 
	 * @param cursor
	 *            passed in cursor
	 * @return Users object
	 */
	public static User getUsersDataFromCursor(Cursor cursor) {

		long userId = cursor.getLong(cursor
				.getColumnIndex(MetaSchema.Users.Cols.ID));	
		String uId = cursor.getString(cursor
				.getColumnIndex(MetaSchema.Users.Cols.UID));
		String emailId = cursor.getString(cursor
				.getColumnIndex(MetaSchema.Users.Cols.EMAIL_ID));
		String phoneNo = cursor.getString(cursor
				.getColumnIndex(MetaSchema.Users.Cols.PHONE_NO));
		String firstName = cursor.getString(cursor
				.getColumnIndex(MetaSchema.Users.Cols.FIRST_NAME));
		String lastName = cursor.getString(cursor
				.getColumnIndex(MetaSchema.Users.Cols.LAST_NAME));
		String createdAt = cursor.getString(cursor
				.getColumnIndex(MetaSchema.Users.Cols.CREATED_AT));
		String authKey = cursor.getString(cursor
				.getColumnIndex(MetaSchema.Users.Cols.AUTH_KEY));
		String isInvitee = cursor.getString(cursor
				.getColumnIndex(MetaSchema.Users.Cols.IS_INVITEE));
		
		// construct the returned object
		User rValue = new User(userId,uId,emailId, phoneNo, firstName, lastName,createdAt,isInvitee,authKey);

		return rValue;
	}

}
