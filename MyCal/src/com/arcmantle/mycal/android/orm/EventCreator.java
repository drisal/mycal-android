package com.arcmantle.mycal.android.orm;

import java.util.ArrayList;
import android.content.ContentValues;
import android.database.Cursor;

import com.arcmantle.mycal.android.provider.MetaSchema;

/**
 * EventCreator is a helper class that does convenience functions for converting
 * between the Custom ORM objects, ContentValues, and Cursors.
 * 
 * @author Deep Risal
 * 
 */
public class EventCreator {

	/**
	 * Create a ContentValues from a provided EventData.
	 * 
	 * @param data
	 *            EventData to be converted.
	 * @return ContentValues that is created from the Event object
	 */
	

	public static ContentValues getCVfromEvent(final Event data) {
		ContentValues rValue = new ContentValues();		
		rValue.put(MetaSchema.Event.Cols.TITLE, data.getTitle());
		rValue.put(MetaSchema.Event.Cols.DESCRIPTION, data.getDescription());
		rValue.put(MetaSchema.Event.Cols.START_TIME,
				data.start);
		rValue.put(MetaSchema.Event.Cols.END_TIME,
				data.end);
		rValue.put(MetaSchema.Event.Cols.CREATION_TIME,
				data.createTime);	
		rValue.put(MetaSchema.Event.Cols.CREATION_TIME,
				data.createTime);	
		rValue.put(MetaSchema.Event.Cols.UPDATE_TIME,
				data.updateTime);
		rValue.put(MetaSchema.Event.Cols.LONGITUDE, data.getLongitude());
		rValue.put(MetaSchema.Event.Cols.LATITUDE, data.getLatitude());
		rValue.put(MetaSchema.Event.Cols.LOCATION, data.getLocation());
		rValue.put(MetaSchema.Event.Cols.COLOR, data.getColor());
		rValue.put(MetaSchema.Event.Cols.CREATED_BY, data.getUserId());
		rValue.put(MetaSchema.Event.Cols.OFF_ACTION, data.getOffAction());
		return rValue;
	}

	/**
	 * Get all of the EventData from the passed in cursor.
	 * 
	 * @param cursor
	 *            passed in cursor to get EventData(s) of.
	 * @return ArrayList<EventData\> The set of EventData
	 */
	public static ArrayList<Event> getEventDataArrayListFromCursor(Cursor cursor) {
		ArrayList<Event> rValue = new ArrayList<Event>();
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {
					rValue.add(getEventDataFromCursor(cursor));
				} while (cursor.moveToNext() == true);
			}
		}
		return rValue;
	}

	/**
	 * Get the first EventData from the passed in cursor.
	 * 
	 * @param cursor
	 *            passed in cursor
	 * @return EventData object
	 */
	public static Event getEventDataFromCursor(Cursor cursor) {
		long rowID = cursor.getLong(cursor
				.getColumnIndex(MetaSchema.Event.Cols.ID));		
		int color = cursor.getInt(cursor
				.getColumnIndex(MetaSchema.Event.Cols.COLOR));
		String title = cursor.getString(cursor
				.getColumnIndex(MetaSchema.Event.Cols.TITLE));
		String description = cursor.getString(cursor
				.getColumnIndex(MetaSchema.Event.Cols.DESCRIPTION));
		String location = cursor.getString(cursor
				.getColumnIndex(MetaSchema.Event.Cols.LOCATION));
		double longitude = cursor.getLong(cursor
				.getColumnIndex(MetaSchema.Event.Cols.LONGITUDE));
		double latitude = cursor.getLong(cursor
				.getColumnIndex(MetaSchema.Event.Cols.LATITUDE));
		long startTime = cursor.getLong(cursor
				.getColumnIndex(MetaSchema.Event.Cols.START_TIME));
		long endTime = cursor.getLong(cursor
				.getColumnIndex(MetaSchema.Event.Cols.END_TIME));
		long createTime = cursor.getLong(cursor
				.getColumnIndex(MetaSchema.Event.Cols.CREATION_TIME));
		long updateTime = cursor.getLong(cursor
				.getColumnIndex(MetaSchema.Event.Cols.UPDATE_TIME));
		long userId = cursor.getLong(cursor
				.getColumnIndex(MetaSchema.Event.Cols.CREATED_BY));
		String offAction = cursor.getString(cursor
				.getColumnIndex(MetaSchema.Event.Cols.OFF_ACTION));
		// construct the returned object
		Event rValue = new Event(color, rowID, title, description, location,
				startTime, endTime, longitude, latitude, createTime,updateTime,userId,offAction);

		return rValue;
	}
}
