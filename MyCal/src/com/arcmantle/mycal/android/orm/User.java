package com.arcmantle.mycal.android.orm;

import java.io.Serializable;

public class User implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long userId;
	private String uId;
	private String emailId;
	private String phoneNo;	
	private String firstName;
	private String lastName;
	private String createdAt;
	private String authKey;
	private boolean invitee;
	private boolean selected=false;
	
	public User(long userId, String uId,String emailId, String phoneNo,
			String firstName, String lastName,String createdAt,String isInvitee,String authKey) {
		super();
		this.userId = userId;
		this.uId=uId;
		this.emailId = emailId;
		this.phoneNo = phoneNo;
		this.firstName = firstName;
		this.lastName = lastName;
		this.createdAt=createdAt;
		this.authKey=authKey;
		if(isInvitee=="0"){
			this.invitee=false;
		}else{
		   this.invitee=true;
		}
	}

	public User(long userId) {		
		this.userId=userId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUsersId(long userId) {
		this.userId = userId;
	}

	public String getuId() {
		return uId;
	}

	public void setuId(String uId) {
		this.uId = uId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getFullName() {
		return firstName+" "+ lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public boolean isInvitee() {
		return invitee;
	}

	public void setInvitee(boolean invitee) {
		this.invitee = invitee;
	}

	public String getAuthKey() {
		return authKey;
	}

	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}	
	

}
