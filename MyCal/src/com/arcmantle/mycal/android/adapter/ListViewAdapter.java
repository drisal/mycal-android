package com.arcmantle.mycal.android.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.arcmantle.mycal.android.R;

public class ListViewAdapter extends BaseAdapter {
	public ArrayList<HashMap> list;
	Activity activity;

	public ListViewAdapter(Activity activity, ArrayList<HashMap> list) {
		super();
		this.activity = activity;
		this.list = list;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	private class ViewHolder {
		TextView blank;
		TextView sunday;
		TextView monday;
		TextView tuesday;
		TextView wednesday;
		TextView thursday;
		TextView friday;
		TextView saturday;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		ViewHolder holder;
		LayoutInflater inflater = activity.getLayoutInflater();

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.listview_row, null);
			holder = new ViewHolder();
			holder.blank=(TextView) convertView.findViewById(R.id.Sunday);
			holder.sunday = (TextView) convertView
					.findViewById(R.id.Sunday);
			holder.monday = (TextView) convertView
					.findViewById(R.id.Monday);
			holder.tuesday = (TextView) convertView
					.findViewById(R.id.Tuesday);
			holder.wednesday = (TextView) convertView
					.findViewById(R.id.Tuesday);
			holder.thursday = (TextView) convertView
					.findViewById(R.id.Thursday);
			holder.friday = (TextView) convertView
					.findViewById(R.id.Friday);
			holder.saturday = (TextView) convertView
					.findViewById(R.id.Saturday);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		HashMap map = list.get(position);
		/*holder.txtFirst.setText(map.get(Constant.FIRST_COLUMN));
		holder.txtSecond.setText(map.get(Constant.SECOND_COLUMN));
		holder.txtThird.setText(map.get(Constant.THIRD_COLUMN));
		holder.txtFourth.setText(map.get(FOURTH_COLUMN));*/

		return convertView;
	}
	
	
}
